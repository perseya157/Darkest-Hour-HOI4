####################################################################
#### Custom Report Headers
####################################################################
defined_text = { 
	name = Dynamic_SOV_17th_Congress
	### Generic
	text = {
		trigger = {
			has_country_flag = kirov_path
		}
		localization_key = GFX_news_SOV_Kirov
	}
	text = {
		trigger = {
			has_country_flag = bukharin_path
		}
		localization_key = GFX_news_SOV_Bukharin
	}
	text = {
		trigger = {
			always = yes
		}
		localization_key = GFX_news_SOV_17th_Congress
	}
}
defined_text = { 
	name = Dynamic_SOV_Public_Loans
	
	text = {
		trigger = {
			check_variable = { var = SOV_Current_Population_Suspicion value = 0 compare = equals }
		}
		localization_key = population_suspicion_none
	}
	text = {
		trigger = {
			check_variable = { var = SOV_Current_Population_Suspicion value = 1 compare = equals }
		}
		localization_key = population_suspicion_very_low
	}
	text = {
		trigger = {
			check_variable = { var = SOV_Current_Population_Suspicion value = 2 compare = equals }
		}
		localization_key = population_suspicion_low
	}
	text = {
		trigger = {
			check_variable = { var = SOV_Current_Population_Suspicion value = 3 compare = equals }
		}
		localization_key = population_suspicion_medium
	}
	text = {
		trigger = {
			check_variable = { var = SOV_Current_Population_Suspicion value = 4 compare = equals }
		}
		localization_key = population_suspicion_high
	}
	text = {
		trigger = {
			check_variable = { var = SOV_Current_Population_Suspicion value = 5 compare = equals }
		}
		localization_key = population_suspicion_very_high
	}
}
defined_text = { 
	name = Dynamic_SOV_Chief_of_NKVD
	
	text = {
		trigger = {
			has_country_flag = SOV_Yagoda_Chief_of_NKVD
		}
		localization_key = Yagoda_chief_of_NKVD
	}
}
defined_text = { 
	name = Dynamic_SOV_Deputy_of_NKVD
	
	text = {
		trigger = {
			has_country_flag = SOV_Agranov_Deputy_of_NKVD
		}
		localization_key = Agranov_deputy_of_NKVD
	}
	text = {
		trigger = {
			has_country_flag = SOV_Frinovsky_Deputy_of_NKVD
		}
		localization_key = Frinovsky_deputy_of_NKVD
	}
}
defined_text = { 
	name = Dynamic_SOV_Purge_Phase_Two
	
	text = {
		trigger = {
			NOT = { has_country_flag = SOV_Purge_Phase_Two_Done }
		}
		localization_key = Phase_Two_Planned
	}
	text = {
		trigger = {
			has_country_flag = SOV_Purge_Phase_Two_Done
		}
		localization_key = Phase_Two_Done
	}
}
defined_text = { 
	name = Dynamic_SOV_Purge_Phase_Three
	
	text = {
		trigger = {
			NOT = {
				has_country_flag = SOV_Purge_Phase_Three_Done
				has_country_flag = SOV_Purge_Phase_Three_Underway
			}
		}
		localization_key = Phase_Three_Planned
	}
	text = {
		trigger = {
			has_country_flag = SOV_Purge_Phase_Three_Underway
		}
		localization_key = Phase_Three_Underway
	}
	text = {
		trigger = {
			has_country_flag = SOV_Purge_Phase_Three_Done
		}
		localization_key = Phase_Three_Done
	}
}
defined_text = { 
	name = Dynamic_SOV_Purge_Phase_Four
	
	text = {
		trigger = {
			NOT = {
				has_country_flag = SOV_Purge_Phase_Four_Underway
			}
		}
		localization_key = Phase_Four_Pending
	}
	text = {
		trigger = {
			has_country_flag = SOV_Purge_Phase_Four_Underway
		}
		localization_key = Phase_Four_Underway
	}
}
