default_peace = {
	enable = {
		always = yes
	}

	annex_randomness = 0
	liberate_randomness = 0
	puppet_randomness = 0
	take_states_randomness = 0
	force_government_randomness = 0
	
	# ROOT = Taker, FROM = Giver
	annex = {
		factor = 10

		#If all states are claimed
		modifier = { 
			factor = 100
			FROM = {
				NOT = {
					any_owned_state = { 
						NOT = {
							OR = { 
								is_claimed_by = ROOT
								is_core_of = ROOT
							}
						}
					}
				}
			}
		}

		#Spanish Civil War
		modifier = { 
			factor = 100
			ROOT = { 
				original_tag = SPA
				has_war_with = SPR
			}
		}

		modifier = { 
			factor = 100
			ROOT = { 
				original_tag = SPR
				has_war_with = SPA
			}
		}
	}

	# ROOT = Taker, FROM = Liberated
	liberate = {
		factor = 1

		modifier = {
			factor = 0
			NOT = {
				ai_liberate_desire = {
					target = FROM
					value > 0
				}
			}
		}
	}

	# ROOT = Taker, FROM = Giver
	puppet = {
		factor = 90
		modifier = { #Don't puppet nations in civil war
			factor = 0
			FROM = { has_civil_war = yes }
		}
		modifier = { #Not if we claim anything
			factor = 0
			FROM = {
				any_owned_state = { 
					AND = { 
						OR = { 
							is_claimed_by = ROOT
							is_core_of = ROOT
						}
						NOT = { has_claimed_state_in_peace_conference = ROOT }
					}
				}
			}
		}
		#Spanish Civil War
		modifier = { 
			factor = 0
			ROOT = { 
				original_tag = SPA
				has_war_with = SPR
			}
		}
		modifier = { 
			factor = 0
			ROOT = { 
				original_tag = SPR
				has_war_with = SPA
			}
		}
		modifier = { #Two-Liu War
			factor = 0
			ROOT = { tag = LXI }
			FROM = { tag = LWH }
		}
	}

	# ROOT = Taker, FROM = Giver
	puppet_all = {
		base = 0 
	}
	
	# ROOT = STATE, FROM = Taker, FROM.FROM = Giver
	puppet_state = {
		base = 0 
		modifier = { #puppet state only if already puppeted
			add = 200
			#only if already puppetted the country
			is_in_array = { subject_countries@FROM = FROM.FROM }
			#only if already puppetted a neighbor state
			any_neighbor_state = {
				is_in_array = { subject_states@FROM = this }
			}
		}
	}

	# ROOT = Taker, FROM = State
	take_states = {
		factor = 50

		modifier = {
			factor = 500
			FROM = {
				OR = {
					is_claimed_by = ROOT
					is_core_of = ROOT
				}
			}
		}

		modifier = {
			factor = 1.25

			capital_scope = {
				distance_to = {
					target = FROM
					value < 100
				}
			}
		}

		modifier = {
			factor = 1.25

			capital_scope = {
				distance_to = {
					target = FROM
					value < 200
				}
			}
		}

		modifier = {
			factor = 1.25

			capital_scope = {
				distance_to = {
					target = FROM
					value < 300
				}
			}
		}

		modifier = {
			factor = 1.25

			capital_scope = {
				distance_to = {
					target = FROM
					value < 400
				}
			}
		}

		modifier = {
			factor = 1.5
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 1
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 1
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 1.5
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 2
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 2
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 1.5
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 3
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 3
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 1.5
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 4
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 4
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 1.5
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 5
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 5
						owner = ROOT
					}
				}
			}
		}

		modifier = { #Only take claims - may also take African colonies if already a colonial power.
			factor = 0
			NOT = { 
				FROM = { 
					OR = {
						is_claimed_by = ROOT
						is_core_of = ROOT
					}
				} 
			}
			NOT = { 
				FROM = { is_on_continent = africa } 
				capital_scope = { is_on_continent = europe }
				any_owned_state = { is_on_continent = africa }
			}
		}
		modifier = { #Only take claimed/core land if a subject.
			factor = 0
			is_subject = yes
			FROM = {
				NOT = {
					OR = { 
						is_claimed_by = ROOT
						is_core_of = ROOT
					}
				}
			}
		}
	}

	# ROOT = Taker, FROM = Giver
	force_government = {
		factor = 0 #Disabled in Darkest Hour
	}
}
