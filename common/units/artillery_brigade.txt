sub_units = {

### Artillery Brigade ---------------------------------------------
	artillery_battalion = {
		sprite = artillery
		map_icon_category = infantry
		priority = 298
		ai_priority = 100
		active = yes
		
		
		type = {
			infantry
			artillery
		}

		group = artillery
		
		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 3

		manpower = 500
		need = {
			artillery_equipment = 12
		}
		
		max_strength = 0.6
		max_organisation = 0
		default_morale = 0.1
		training_time = 120
		weight = 0.5
		supply_consumption = 0.2

		#Line artillery bonuses
		soft_attack = 0.25
		
		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		jungle = {
			attack = -0.25
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
	artillery_battalion_light = {
		sprite = artillery
		map_icon_category = infantry
		priority = 298
		ai_priority = 100
		active = yes
		
		
		type = {
			infantry
			artillery
		}

		group = artillery
		
		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 2.5

		manpower = 500
		need = {
			artillery_equipment = 12
		}
		
		max_strength = 0.6
		max_organisation = 0
		default_morale = 0.1
		training_time = 120
		weight = 0.5
		supply_consumption = 0.2

		#Line artillery bonuses
		soft_attack = 0.25
		
		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		jungle = {
			attack = -0.25
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
	artillery_battalion_heavy = {
		sprite = artillery
		map_icon_category = infantry
		priority = 298
		ai_priority = 100
		active = no
		
		
		type = {
			infantry
			artillery
		}

		group = artillery
		
		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 4

		manpower = 500
		need = {
			artillery_equipment = 12
		}
		
		max_strength = 0.6
		max_organisation = 0
		default_morale = 0.1
		training_time = 120
		weight = 0.5
		supply_consumption = 0.2

		#Line artillery bonuses
		soft_attack = 0.25
		
		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		jungle = {
			attack = -0.25
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}

	###  Mot. Artillery Brigade ---------------------------------------------
	motorized_artillery_battalion = {
		sprite = artillery
		map_icon_category = infantry
		priority = 298
		ai_priority = 100
		active = no
		
		
		type = {
			infantry
			artillery
		}

		group = artillery
		
		categories = {
			category_army
			category_line_artillery
			category_artillery
		}

		combat_width = 3

		manpower = 550
		transport = motorized_equipment
		
		need = {
			artillery_equipment = 12
			motorized_equipment = 40		
		}
		
		max_strength = 0.6
		max_organisation = 0
		default_morale = 0.1
		training_time = 120
		weight = 0.5
		supply_consumption = 0.2

		#Line artillery bonuses
		soft_attack = 0.25
		
		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		jungle = {
			attack = -0.25
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}	
	
###  Rocket Artillery Brigade ---------------------------------------------	
	rocket_artillery_battalion = {
		sprite = "artillery"
		map_icon_category = infantry
		priority = 299
		ai_priority = 100
		active = yes
		
		type = {
			infantry
			artillery
		}

		group = artillery
		
		categories = {
			category_army
			category_line_artillery
		}		

		combat_width = 3

		need = {
			rocket_artillery_equipment = 12
		}
		
		manpower = 500
		max_organisation = 0
		default_morale = 0.1
		max_strength = 0.6
		training_time = 120
		weight = 0.5

		supply_consumption = 0.2
		
		forest = {
			attack = -0.2
			movement = -0.2
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		jungle = {
			attack = -0.25
			movement = -0.2
		}

		marsh = {
			attack = -0.2
			movement = -0.3
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
###  Mot. Rocket Artillery Brigade ---------------------------------------------	
	motorized_rocket_brigade = {
		sprite = motorized
		map_icon_category = infantry
		priority = 299
		ai_priority = 100
		active = no
		
		type = {
			artillery
			motorized
			rocket
		}

		group = artillery
		
		categories = {
			category_army
			category_line_artillery
		}		

		combat_width = 3

		need = {
			motorized_rocket_equipment = 20
			motorized_equipment = 15
		}
		
		manpower = 550
		max_organisation = 0
		default_morale = 0.1
		max_strength = 0.6
		training_time = 120
		weight = 0.5

		
		supply_consumption = 0.28
		
		forest = {
			attack = -0.1
			movement = -0.5
		}

		hills = {
			movement = -0.05
		}

		mountain = {
			movement = -0.2
		}

		jungle = {
			attack = -0.25
			movement = -0.5
		}

		marsh = {
			attack = -0.2
			movement = -0.5
		}

		fort = {
			attack = 0.1
		}

		river = {
			attack = -0.2
			movement = -0.2
		}

		amphibious = {
			attack = -0.4
		}
	}
}
