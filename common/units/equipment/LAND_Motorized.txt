equipments = {
	Motorized_equipment = {
		year = 1916
		is_archetype = yes
		picture = archetype_motorized_equipment		
		is_buildable = no
		type = {
			#infantry #Removing inf type 
			motorized
		}
		group_by = archetype
		interface_category = interface_category_land
		maximum_speed = 12
		reliability = 0.8
		hardness = 0.1
		#Space taken in convoy
		lend_lease_cost = 5
		build_cost_ic = 2.5
		resources = {
			rubber = 1
			steel = 1
		}
		fuel_consumption = 1.2
	}
	Motorized_equipment_1916 = {
		year = 1916
		archetype = Motorized_equipment
		priority = 30			
	}
	Motorized_equipment_1932 = {
		year = 1916
		archetype = Motorized_equipment
		priority = 30			
		parent = Motorized_equipment_1916
	}
	Motorized_equipment_1938 = {
		year = 1916
		archetype = Motorized_equipment
		priority = 30			
		parent = Motorized_equipment_1932
	}
}
