ideas = {
	#################################################
	### Head of Government
	#################################################
	Head_of_Government = {
		# Ganden Tripa Isomolin Rimpoche
		TIB_D_Tenzin_Gyatso = {
			picture = Tenzin_Gyatso
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenzin_Gyatso_unavailable
				}
			}
			traits = {
				ideology_A
				POSITION_Dalai_Lama
				L_Benevolent_Gentleman
			}
		}
		# Ganden Tripa Isomolin Rimpoche
		TIB_D_Thubten_Gyatso = {
			picture = Thubten_Gyatso
			allowed = {
				tag = TIB
			}
			available = {
				date > 1910.1.1
				date < 1934.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Thubten_Gyatso_unavailable
				}
			}
			traits = {
				ideology_A
				POSITION_Dalai_Lama
				L_Naive_Optimist
			}
		}
		# Ganden Tripa Isomolin Rimpoche
		TIB_D_Ganden_Tripa_Isomolin_Rimpoche = {
			picture = Ganden_Tripa_Isomolin_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Ganden_Tripa_Isomolin_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				L_Naive_Optimist
			}
		}
		# Gandain Chiba Lobsang Gyaincain
		TIB_D_Gandain_Chiba_Lobsang_Gyaincain = {
			picture = Gandain_Chiba_Lobsang_Gyaincain
			allowed = {
				tag = TIB
			}
			available = {
				date > 1904.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gandain_Chiba_Lobsang_Gyaincain_unavailable
				}
			}
			traits = {
				ideology_A
				L_Naive_Optimist
			}
		}
		# Lonchen Paljor Dorje Shatra
		TIB_D_Lonchen_Paljor_Dorje_Shatra = {
			picture = Lonchen_Paljor_Dorje_Shatra
			allowed = {
				tag = TIB
			}
			available = {
				date > 1907.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Lonchen_Paljor_Dorje_Shatra_unavailable
				}
			}
			traits = {
				ideology_A
				L_Silent_Workhorse
			}
		}
		# Sholkhang
		TIB_D_Sholkhang = {
			picture = Sholkhang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1907.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sholkhang_unavailable
				}
			}
			traits = {
				ideology_A
				L_Silent_Workhorse
			}
		}
		# Chikyak Khenpo Champa Tubwang
		TIB_D_Chikyak_Khenpo_Champa_Tubwang = {
			picture = Chikyak_Khenpo_Champa_Tubwang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1913.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chikyak_Khenpo_Champa_Tubwang_unavailable
				}
			}
			traits = {
				ideology_A
				L_Backroom_Backstabber
			}
		}
		# Reting Rimpoche
		TIB_D_Reting_Rimpoche = {
			picture = Reting_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Reting_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				L_Old_General
			}
		}
		# Tenzin Tethong
		TIB_D_Tenzin_Tethong = {
			picture = Tenzin_Tethong
			allowed = {
				tag = TIB
			}
			available = {
				date > 1991.1.1
				date < 2020.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenzin_Tethong_unavailable
				}
			}
			traits = {
				ideology_A
				L_Silent_Workhorse
			}
		}
		# Tsarong Dzasa
		TIB_D_Tsarong_Dzasa = {
			picture = Tsarong_Dzasa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1924.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsarong_Dzasa_unavailable
				}
			}
			traits = {
				ideology_D
				L_Old_General
			}
		}
		# Taktra Rinpoche
		TIB_D_Taktra_Rinpoche = {
			picture = Taktra_Rinpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Taktra_Rinpoche_unavailable
				}
			}
			traits = {
				ideology_D
				L_Backroom_Backstabber
			}
		}
		# Tenzin Jigme
		TIB_D_Tenzin_Jigme = {
			picture = Tenzin_Jigme
			allowed = {
				tag = TIB
			}
			available = {
				date > 1951.1.1
				date < 1998.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenzin_Jigme_unavailable
				}
			}
			traits = {
				ideology_D
				L_Corporate_Suit
			}
		}
		# Phuntsog Wangyal
		TIB_D_Phuntsog_Wangyal = {
			picture = Phuntsog_Wangyal
			allowed = {
				tag = TIB
			}
			available = {
				date > 1959.1.1
				date < 2020.1.1
				C_Minister_Allowed = yes
				NOT = {
					has_country_flag = Phuntsog_Wangyal_unavailable
				}
			}
			traits = {
				ideology_C
				L_Corporate_Suit
			}
		}
	}
	#################################################
	### Foreign Minister
	#################################################
	Foreign_Minister = {
		# Agvan Drozhiev
		TIB_FM_Agvan_Drozhiev = {
			picture = Agvan_Drozhiev
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Agvan_Drozhiev_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Great_Compromiser
			}
		}
		# Silun Qamqen
		TIB_FM_Silun_Qamqen = {
			picture = Silun_Qamqen
			allowed = {
				tag = TIB
			}
			available = {
				date > 1912.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Silun_Qamqen_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Biased_Intellectual
			}
		}
		# Gudjir Lubsan-Agvan Choinzin
		TIB_FM_Gudjir_LubsanAgvan_Choinzin = {
			picture = Gudjir_LubsanAgvan_Choinzin
			allowed = {
				tag = TIB
			}
			available = {
				date > 1913.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gudjir_LubsanAgvan_Choinzin_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Ideological_Crusader
			}
		}
		# Reting Rimpoche
		TIB_FM_Reting_Rimpoche = {
			picture = Reting_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Reting_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Apologetic_Clerk
			}
		}
		# Kuntsig Shamar Rimpoche
		TIB_FM_Kuntsig_Shamar_Rimpoche = {
			picture = Kuntsig_Shamar_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kuntsig_Shamar_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Ideological_Crusader
			}
		}
		# Taktra Rinpoche
		TIB_FM_Taktra_Rinpoche = {
			picture = Taktra_Rinpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Taktra_Rinpoche_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Great_Compromiser
			}
		}
		# Gyato Wangdu
		TIB_FM_Gyato_Wangdu = {
			picture = Gyato_Wangdu
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gyato_Wangdu_unavailable
				}
			}
			traits = {
				ideology_A
				FM_General_Staffer
			}
		}
		# Gangdhjom Tharchin
		TIB_FM_Gangdhjom_Tharchin = {
			picture = Gangdhjom_Tharchin
			allowed = {
				tag = TIB
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gangdhjom_Tharchin_unavailable
				}
			}
			traits = {
				ideology_A
				FM_The_Cloak_N_Dagger_Schemer
			}
		}
		# Gedun Choephel
		TIB_FM_Gedun_Choephel = {
			picture = Gedun_Choephel
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gedun_Choephel_unavailable
				}
			}
			traits = {
				ideology_D
				FM_Biased_Intellectual
			}
		}
		# Jigme Taring
		TIB_FM_Jigme_Taring = {
			picture = Jigme_Taring
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Jigme_Taring_unavailable
				}
			}
			traits = {
				ideology_D
				FM_Apologetic_Clerk
			}
		}
		# Dudjom Rimpoche
		TIB_FM_Dudjom_Rimpoche = {
			picture = Dudjom_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Dudjom_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_D
				FM_Iron_Fisted_Brute
			}
		}
	}
	#################################################
	### Minister of Security
	#################################################
	Minister_of_Security = {
		# Duiboin Minglingba
		TIB_MoS_Duiboin_Minglingba = {
			picture = Duiboin_Minglingba
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Duiboin_Minglingba_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Crime_Fighter
			}
		}
		# Sholkhang
		TIB_MoS_Sholkhang = {
			picture = Sholkhang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1907.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sholkhang_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Crime_Fighter
			}
		}
		# Chikyak Khenpo Champa Tubwang
		TIB_MoS_Chikyak_Khenpo_Champa_Tubwang = {
			picture = Chikyak_Khenpo_Champa_Tubwang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1913.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chikyak_Khenpo_Champa_Tubwang_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Compassionate_Gentleman
			}
		}
		# Charong Dasang Zhamdui
		TIB_MoS_Charong_Dasang_Zhamdui = {
			picture = Charong_Dasang_Zhamdui
			allowed = {
				tag = TIB
			}
			available = {
				date > 1914.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Charong_Dasang_Zhamdui_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Man_Of_The_People
			}
		}
		# Dorgar Puncog Raogyi
		TIB_MoS_Dorgar_Puncog_Raogyi = {
			picture = Dorgar_Puncog_Raogyi
			allowed = {
				tag = TIB
			}
			available = {
				date > 1922.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Dorgar_Puncog_Raogyi_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Silent_Lawyer
			}
		}
		# Reting Rimpoche
		TIB_MoS_Reting_Rimpoche = {
			picture = Reting_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Reting_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Compassionate_Gentleman
			}
		}
		# Chama Samphe
		TIB_MoS_Chama_Samphe = {
			picture = Chama_Samphe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chama_Samphe_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Efficient_Sociopath
			}
		}
		# Taktra Rinpoche
		TIB_MoS_Taktra_Rinpoche = {
			picture = Taktra_Rinpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Taktra_Rinpoche_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Compassionate_Gentleman
			}
		}
		# Thepon Rananpoi
		TIB_MoS_Thepon_Rananpoi = {
			picture = Thepon_Rananpoi
			allowed = {
				tag = TIB
			}
			available = {
				date > 1939.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Thepon_Rananpoi_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Crime_Fighter
			}
		}
		# Tsepon Shakabpa
		TIB_MoS_Tsepon_Shakabpa = {
			picture = Tsepon_Shakabpa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsepon_Shakabpa_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Prince_Of_Terror
			}
		}
		# Chama Sanampo
		TIB_MoS_Chama_Sanampo = {
			picture = Chama_Sanampo
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chama_Sanampo_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Back_Stabber
			}
		}
		# Bapa Yeshe
		TIB_MoS_Bapa_Yeshe = {
			picture = Bapa_Yeshe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Bapa_Yeshe_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Man_Of_The_People
			}
		}
		# Gedun Choephel
		TIB_MoS_Gedun_Choephel = {
			picture = Gedun_Choephel
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gedun_Choephel_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Silent_Lawyer
			}
		}
	}
	#################################################
	### Armaments Minister
	#################################################
	Armaments_Minister = {
		# Xoikang Cedain Wangqug
		TIB_AM_Xoikang_Cedain_Wangqug = {
			picture = Xoikang_Cedain_Wangqug
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Xoikang_Cedain_Wangqug_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Administrative_Genius
			}
		}
		# Ganden Tripa Isomolin Rimpoche
		TIB_AM_Ganden_Tripa_Isomolin_Rimpoche = {
			picture = Ganden_Tripa_Isomolin_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Ganden_Tripa_Isomolin_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Resource_Industrialist
			}
		}
		# Lungshar Kusho
		TIB_AM_Lungshar_Kusho = {
			picture = Lungshar_Kusho
			allowed = {
				tag = TIB
			}
			available = {
				date > 1912.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Lungshar_Kusho_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Resource_Industrialist
			}
		}
		# Gudjir Lubsan-Agvan Choinzin
		TIB_AM_Gudjir_LubsanAgvan_Choinzin = {
			picture = Gudjir_LubsanAgvan_Choinzin
			allowed = {
				tag = TIB
			}
			available = {
				date > 1913.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gudjir_LubsanAgvan_Choinzin_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Corrupt_Kleptocrat
			}
		}
		# Tsepon Shakabpa
		TIB_AM_Tsepon_Shakabpa = {
			picture = Tsepon_Shakabpa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsepon_Shakabpa_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Corrupt_Kleptocrat
			}
		}
		# Reting Rimpoche
		TIB_AM_Reting_Rimpoche = {
			picture = Reting_Rimpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Reting_Rimpoche_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Laissez-faire_Capitalist
			}
		}
		# Khilsin Tchansal
		TIB_AM_Khilsin_Tchansal = {
			picture = Khilsin_Tchansal
			allowed = {
				tag = TIB
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Khilsin_Tchansal_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Theoretical_Scientist
			}
		}
		# Chama Samphe
		TIB_AM_Chama_Samphe = {
			picture = Chama_Samphe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chama_Samphe_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Battle_Fleet_Proponent
			}
		}
		# Tenpa Jamyang
		TIB_AM_Tenpa_Jamyang = {
			picture = Tenpa_Jamyang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenpa_Jamyang_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Infantry_Proponent
			}
		}
		# Gyato Wangdu
		TIB_AM_Gyato_Wangdu = {
			picture = Gyato_Wangdu
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gyato_Wangdu_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Tank_Proponent
			}
		}
		# Sampo Tsewang Rigzin
		TIB_AM_Sampo_Tsewang_Rigzin = {
			picture = Sampo_Tsewang_Rigzin
			allowed = {
				tag = TIB
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sampo_Tsewang_Rigzin_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Air_To_Ground_Proponent
			}
		}
		# W.G. Kundeling
		TIB_AM_WG_Kundeling = {
			picture = WG_Kundeling
			allowed = {
				tag = TIB
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = WG_Kundeling_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Administrative_Genius
			}
		}
		# Tsarong Dzasa
		TIB_AM_Tsarong_Dzasa = {
			picture = Tsarong_Dzasa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1912.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsarong_Dzasa_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Military_Entrepreneur
			}
		}
		# Lhalu Tsewang Dorje
		TIB_AM_Lhalu_Tsewang_Dorje = {
			picture = Lhalu_Tsewang_Dorje
			allowed = {
				tag = TIB
			}
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Lhalu_Tsewang_Dorje_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Submarine_Proponent
			}
		}
		# Thupthen Ningee
		TIB_AM_Thupthen_Ningee = {
			picture = Thupthen_Ningee
			allowed = {
				tag = TIB
			}
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Thupthen_Ningee_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Resource_Industrialist
			}
		}
		# Bapa Yeshe
		TIB_AM_Bapa_Yeshe = {
			picture = Bapa_Yeshe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Bapa_Yeshe_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Air_Superiority_Proponent
			}
		}
		# Jigme Taring
		TIB_AM_Jigme_Taring = {
			picture = Jigme_Taring
			allowed = {
				tag = TIB
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Jigme_Taring_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Military_Entrepreneur
			}
		}
	}
	#################################################
	### Head of Intelligence
	#################################################
	Head_of_Intelligence = {
		# Langserling
		TIB_HoI_Langserling = {
			picture = Langserling
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Langserling_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Dismal_Enigma
			}
		}
		# Kalon Lama
		TIB_HoI_Kalon_Lama = {
			picture = Kalon_Lama
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kalon_Lama_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Political_Specialist
			}
		}
		# W.G. Kundeling
		TIB_HoI_WG_Kundeling = {
			picture = WG_Kundeling
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = WG_Kundeling_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Logistics_Specialist
			}
		}
		# Tenpa Jamyang
		TIB_HoI_Tenpa_Jamyang = {
			picture = Tenpa_Jamyang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenpa_Jamyang_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Logistics_Specialist
			}
		}
		# Khilsin Tchansal
		TIB_HoI_Khilsin_Tchansal = {
			picture = Khilsin_Tchansal
			allowed = {
				tag = TIB
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Khilsin_Tchansal_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Technical_Specialist
			}
		}
		# Chama Samphe
		TIB_HoI_Chama_Samphe = {
			picture = Chama_Samphe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chama_Samphe_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Political_Specialist
			}
		}
		# Tsarong Dzasa
		TIB_HoI_Tsarong_Dzasa = {
			picture = Tsarong_Dzasa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1908.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsarong_Dzasa_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Logistics_Specialist
			}
		}
		# Thupthen Ningee
		TIB_HoI_Thupthen_Ningee = {
			picture = Thupthen_Ningee
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Thupthen_Ningee_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Industrial_Specialist
			}
		}
		# Chama Sanampo
		TIB_HoI_Chama_Sanampo = {
			picture = Chama_Sanampo
			allowed = {
				tag = TIB
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chama_Sanampo_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Dismal_Enigma
			}
		}
	}
	#################################################
	### Chief of Staff
	#################################################
	Chief_of_Staff = {
		# Lama Qamqen Ngawang Baisang
		TIB_CoStaff_Lama_Qamqen_Ngawang_Baisang = {
			picture = Lama_Qamqen_Ngawang_Baisang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Lama_Qamqen_Ngawang_Baisang_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Mass_Combat
			}
		}
		# Kalon Lama
		TIB_CoStaff_Kalon_Lama = {
			picture = Kalon_Lama
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kalon_Lama_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Fire_Support
			}
		}
		# Gyato Wangdu
		TIB_CoStaff_Gyato_Wangdu = {
			picture = Gyato_Wangdu
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gyato_Wangdu_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Manoeuvre
			}
		}
		# Tenpa Jamyang
		TIB_CoStaff_Tenpa_Jamyang = {
			picture = Tenpa_Jamyang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenpa_Jamyang_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Fire_Support
			}
		}
		# Bapa Yeshe
		TIB_CoStaff_Bapa_Yeshe = {
			picture = Bapa_Yeshe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Bapa_Yeshe_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Manoeuvre
			}
		}
		# Tsarong Dzasa
		TIB_CoStaff_Tsarong_Dzasa = {
			picture = Tsarong_Dzasa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1912.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsarong_Dzasa_unavailable
				}
			}
			traits = {
				ideology_D
				CoStaff_School_Of_Psychology
			}
		}
		# Andruk Gonpo Tashi
		TIB_CoStaff_Andruk_Gonpo_Tashi = {
			picture = Andruk_Gonpo_Tashi
			allowed = {
				tag = TIB
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Andruk_Gonpo_Tashi_unavailable
				}
			}
			traits = {
				ideology_D
				CoStaff_School_Of_Psychology
			}
		}
		# Jampal Rabgyé Rinpoche
		TIB_CoStaff_Jampal_Rabgye_Rinpoche = {
			picture = Jampal_Rabgye_Rinpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Jampal_Rabgye_Rinpoche_unavailable
				}
			}
			traits = {
				ideology_D
				CoStaff_School_Of_Defence
			}
		}
	}
	#################################################
	### Chief of Army
	#################################################
	Chief_of_Army = {
		# Xazha Benjor Doje
		TIB_CoArmy_Xazha_Benjor_Doje = {
			picture = Xazha_Benjor_Doje
			allowed = {
				tag = TIB
			}
			available = {
				date > 1900.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Xazha_Benjor_Doje_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Decisive_Battle_Doctrine
			}
		}
		# Kalon Lama
		TIB_CoArmy_Kalon_Lama = {
			picture = Kalon_Lama
			allowed = {
				tag = TIB
			}
			available = {
				date > 1904.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kalon_Lama_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Guns_And_Butter_Doctrine
			}
		}
		# Changra Depsn
		TIB_CoArmy_Changra_Depsn = {
			picture = Changra_Depsn
			allowed = {
				tag = TIB
			}
			available = {
				date > 1910.1.1
				date < 1933.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Changra_Depsn_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Static_Defence_Doctrine
			}
		}
		# Tenpa Jamyang
		TIB_CoArmy_Tenpa_Jamyang = {
			picture = Tenpa_Jamyang
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tenpa_Jamyang_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Elastic_Defence_Doctrine
			}
		}
		# Bapa Yeshe
		TIB_CoArmy_Bapa_Yeshe = {
			picture = Bapa_Yeshe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Bapa_Yeshe_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Elastic_Defence_Doctrine
			}
		}
		# Chaghoe Namgyal Dorje
		TIB_CoArmy_Chaghoe_Namgyal_Dorje = {
			picture = Chaghoe_Namgyal_Dorje
			allowed = {
				tag = TIB
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chaghoe_Namgyal_Dorje_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Decisive_Battle_Doctrine
			}
		}
		# Gyato Wangdu
		TIB_CoArmy_Gyato_Wangdu = {
			picture = Gyato_Wangdu
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gyato_Wangdu_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Armoured_Spearhead_Doctrine
			}
		}
		# Khemey Sonam Wangdi
		TIB_CoArmy_Khemey_Sonam_Wangdi = {
			picture = Khemey_Sonam_Wangdi
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Khemey_Sonam_Wangdi_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Guns_And_Butter_Doctrine
			}
		}
		# Tsarong Dzasa
		TIB_CoArmy_Tsarong_Dzasa = {
			picture = Tsarong_Dzasa
			allowed = {
				tag = TIB
			}
			available = {
				date > 1912.1.1
				date < 1933.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Tsarong_Dzasa_unavailable
				}
			}
			traits = {
				ideology_D
				CoArmy_Decisive_Battle_Doctrine
			}
		}
		# Jampal Rabgyé Rinpoche
		TIB_CoArmy_Jampal_Rabgye_Rinpoche = {
			picture = Jampal_Rabgye_Rinpoche
			allowed = {
				tag = TIB
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Jampal_Rabgye_Rinpoche_unavailable
				}
			}
			traits = {
				ideology_D
				CoArmy_Static_Defence_Doctrine
			}
		}
	}
	#################################################
	### Chief of Navy
	#################################################
	Chief_of_Navy = {
		# Chama Samphe
		TIB_CoNavy_Chama_Samphe = {
			picture = Chama_Samphe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chama_Samphe_unavailable
				}
			}
			traits = {
				ideology_A
				CoNavy_Decisive_Naval_Battle_Doctrine
			}
		}
		# Chaghoe Namgyal Dorje
		TIB_CoNavy_Chaghoe_Namgyal_Dorje = {
			picture = Chaghoe_Namgyal_Dorje
			allowed = {
				tag = TIB
			}
			available = {
				date > 1939.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Chaghoe_Namgyal_Dorje_unavailable
				}
			}
			traits = {
				ideology_A
				CoNavy_Open_Seas_Doctrine
			}
		}
		# Lhalu Tsewang Dorje
		TIB_CoNavy_Lhalu_Tsewang_Dorje = {
			picture = Lhalu_Tsewang_Dorje
			allowed = {
				tag = TIB
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Lhalu_Tsewang_Dorje_unavailable
				}
			}
			traits = {
				ideology_D
				CoNavy_Indirect_Approach_Doctrine
			}
		}
	}
	#################################################
	### Chief of Airforce
	#################################################
	Chief_of_Airforce = {
		# Sampo Tsewang Rigzin
		TIB_CoAir_Sampo_Tsewang_Rigzin = {
			picture = Sampo_Tsewang_Rigzin
			allowed = {
				tag = TIB
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sampo_Tsewang_Rigzin_unavailable
				}
			}
			traits = {
				ideology_A
				CoAir_Army_Aviation_Doctrine
			}
		}
		# Gyato Wangdu
		TIB_CoAir_Gyato_Wangdu = {
			picture = Gyato_Wangdu
			allowed = {
				tag = TIB
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gyato_Wangdu_unavailable
				}
			}
			traits = {
				ideology_A
				CoAir_Army_Aviation_Doctrine
			}
		}
		# Bapa Yeshe
		TIB_CoAir_Bapa_Yeshe = {
			picture = Bapa_Yeshe
			allowed = {
				tag = TIB
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Bapa_Yeshe_unavailable
				}
			}
			traits = {
				ideology_D
				CoAir_Air_Superiority_Doctrine
			}
		}
	}
}
