ideas = {
	country = {
		bankruptcy = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.2
				war_support_factor = -0.25
				max_command_power = -50
				surrender_limit = -0.25
				political_power_cost = 1
				trade_opinion_factor = -0.5
				army_org_factor = -0.1
				production_factory_max_efficiency_factor = -0.2
				production_speed_buildings_factor = -0.2
				industrial_capacity_factory = -0.2
				industrial_capacity_dockyard = -0.2
				consumer_goods_factor = 0.1
				conversion_cost_civ_to_mil_factor = 1
				inflation_change = -0.01
			}
			picture = "ger_mefo_bills_payment"
		}
		generic_unemployment_idea = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			picture = "GER_Unemployment_Focus"
		}
	}
	taxation_slider_law = {
		law = yes
		use_list_view = yes
		# Taxation Level:
		taxation_level_1 = {
			picture = GENERIC_Minimal_Taxes
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = 0.1
				political_power_gain = 0.1
				research_speed_factor = 0.1
				production_speed_buildings_factor = 0.1
				industrial_capacity_factory = 0.05
				industrial_capacity_dockyard = 0.05
				production_factory_max_efficiency_factor = 0.05
				production_factory_efficiency_gain_factor = 0.15
				consumer_goods_factor = 0.05
				tax_efficiency_factor = -0.55
			}
		}
		taxation_level_2 = {
			picture = GENERIC_Low_Taxes
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.05
				research_speed_factor = 0.05
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.025
				industrial_capacity_dockyard = 0.025
				production_factory_efficiency_gain_factor = 0.1
				tax_efficiency_factor = -0.3
			}
		}
		taxation_level_3 = {
			picture = GENERIC_Medium_Taxes
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.02
				political_power_gain = 0.02
				production_speed_buildings_factor = -0.05
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
				production_factory_max_efficiency_factor = -0.025
				production_factory_efficiency_gain_factor = 0.05
			}
		}
		taxation_level_4 = {
			picture = GENERIC_High_Taxes
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.05
				political_power_gain = -0.1
				research_speed_factor = -0.075
				production_speed_buildings_factor = -0.075
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.10
				production_factory_max_efficiency_factor = -0.05
				production_factory_efficiency_gain_factor = -0.1
				tax_efficiency_factor = 0.35
			}
		}
		taxation_level_5 = {
			picture = GENERIC_Death_and_Taxes
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.15
				political_power_gain = -0.2
				research_speed_factor = -0.165
				production_speed_buildings_factor = -0.16
				industrial_capacity_factory = -0.24
				industrial_capacity_dockyard = -0.15
				production_factory_max_efficiency_factor = -0.15
				production_factory_efficiency_gain_factor = -0.15
				tax_efficiency_factor = 0.7
			}
		}
	}
	social_spending_slider_law = {
		law = yes
		use_list_view = yes
		# Social Spending Level:
		social_spending_level_1 = {
			picture = GENERIC_No_Social
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.15
				war_support_factor = -0.05
				political_power_factor = -0.2
				monthly_population = -0.3
				drift_defence_factor = -0.3
				social_spending_cost_factor = -0.8
			}
		}
		social_spending_level_2 = {
			picture = GENERIC_Minimal_Social
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.075
				war_support_factor = -0.03
				political_power_factor = -0.15
				monthly_population = -0.15
				drift_defence_factor = -0.15
				social_spending_cost_factor = -0.5
			}
		}
		social_spending_level_3 = {
			picture = GENERIC_Low_Social
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.05
				war_support_factor = -0.02
				political_power_factor = -0.075
				monthly_population = -0.05
				drift_defence_factor = -0.05
			}
		}
		social_spending_level_4 = {
			picture = GENERIC_Generous_Social
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = 0.05
				political_power_factor = 0.05
				monthly_population = 0.15
				drift_defence_factor = 0.15
				social_spending_cost_factor = 1
			}
		}
		social_spending_level_5 = {
			picture = GENERIC_Large_Social
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = 0.1
				political_power_factor = 0.15
				monthly_population = 0.25
				drift_defence_factor = 0.2
				production_speed_buildings_factor = -0.05
				social_spending_cost_factor = 4
			}
		}
	}
	research_spending_slider_law = {
		law = yes
		use_list_view = yes
		# Research and Education Spending Level:
		research_spending_level_1 = {
			picture = GENERIC_Limited_Research
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				research_speed_factor = -0.3
				stability_factor = -0.1
				political_power_factor = -0.12
				monthly_population = 0.2
				research_spending_cost_factor = -0.7
			}
		}
		research_spending_level_2 = {
			picture = GENERIC_Subsidied_Research
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				research_speed_factor = -0.2
				stability_factor = -0.05
				political_power_factor = -0.075
				monthly_population = 0.15
				research_spending_cost_factor = -0.4
			}
		}
		research_spending_level_3 = {
			picture = GENERIC_Medium_Research
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				stability_factor = -0.02
				political_power_factor = -0.025
				monthly_population = 0.1
			}
		}
		research_spending_level_4 = {
			picture = GENERIC_Extensive_Research
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				research_speed_factor = 0.15
				stability_factor = 0.03
				political_power_factor = 0.03
				monthly_population = -0.1
				research_spending_cost_factor = 0.5
			}
		}
		research_spending_level_5 = {
			picture = GENERIC_Massive_Research
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				research_speed_factor = 0.25
				stability_factor = 0.05
				political_power_factor = 0.1
				monthly_population = -0.2
				research_spending_cost_factor = 1
			}
		}
	}
	army_spending_slider_law = {
		law = yes
		use_list_view = yes
		# Army Spending Level:
		army_spending_level_1 = {
			picture = GENERIC_Militia_Army
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				army_org_factor = -0.4
				training_time_army_factor = 0.3
				special_forces_min = 2
				conscription_factor = -0.7
				army_spending_cost_factor = -0.1
			}
		}
		army_spending_level_2 = {
			picture = GENERIC_Disarmed_Army
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				army_org_factor = -0.3
				training_time_army_factor = 0.2
				minimum_training_level = -0.1
				mobilization_speed = 0.5
				army_morale_factor = -0.15
				land_reinforce_rate = 0.02
				conscription_factor = 0.2
				army_defence_factor = 0.1
				army_spending_cost_factor = -0.05
			}
		}
		army_spending_level_3 = {
			picture = GENERIC_Standing_Army
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				special_forces_cap = 0.01
				experience_gain_factor = 0.05
			}
		}
		army_spending_level_4 = {
			picture = GENERIC_Increased_Army
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				army_org_factor = 0.05
				army_morale_factor = 0.05
				land_reinforce_rate = 0.01
				special_forces_cap = 0.03
				mobilization_speed = 1
				experience_gain_factor = 0.1
				army_attack_factor = 0.05
				army_defence_factor = 0.05
				army_spending_cost_factor = 0.1
			}
		}
		army_spending_level_5 = {
			picture = GENERIC_Large_Army
			removal_cost = 0
			cancel_if_invalid = no
			cost = 0
			allowed_to_remove = {
				custom_trigger_tooltip = {
					tooltip = CANNOT_CHANGE_SLIDER_LAW_MUST_USE_SLIDER
					is_ai = yes
				}
			}
			modifier = {
				army_org_factor = 0.15
				army_morale_factor = 0.1
				land_reinforce_rate = 0.02
				special_forces_cap = 0.05
				mobilization_speed = 1.5
				experience_gain_factor = 0.15
				army_attack_factor = 0.1
				army_defence_factor = 0.05
				army_spending_cost_factor = 0.25
			}
		}
	}
}