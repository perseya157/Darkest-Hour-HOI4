ideas = {
	#################################################
	### Head of Government
	#################################################
	Head_of_Government = {
		# Helmer Rosting
		DNZ_D_Helmer_Rosting = {
			picture = D_Helmer_Rosting
			allowed = {
				tag = DNZ
			}
			allowed_to_remove = {
				always = no
			}
			available = {
				NOT = {
					has_country_flag = Helmer_Rosting_unavailable
				}
			}
			visible = {
				date < 1945.6.28
			}
			traits = {
				POSITION_High_Commissioner
				ideology_D
				L_Barking_Buffoon
			}
		}
		# Seán Lester
		DNZ_D_Sean_Lester = {
			picture = D_Sean_Lester
			allowed = {
				tag = DNZ
			}
			allowed_to_remove = {
				always = no
			}
			available = {
				NOT = {
					has_country_flag = Sean_Lester_unavailable
				}
			}
			visible = {
				date < 1959.6.13
			}
			traits = {
				POSITION_High_Commissioner
				ideology_D
				L_Popular_Figurehead
			}
		}
		# Carl Jakob Burckhardt
		DNZ_D_Carl_Jakob_Burckhardt = {
			picture = D_Carl_Jakob_Burckhardt
			allowed = {
				tag = DNZ
			}
			allowed_to_remove = {
				always = no
			}
			available = {
				NOT = {
					has_country_flag = Carl_Jakob_Burckhardt_unavailable
				}
			}
			visible = {
				date < 1974.3.3
			}
			traits = {
				POSITION_High_Commissioner
				ideology_D
				L_Benevolent_Gentleman
			}
		}
	}
	#################################################
	### Armaments Minister
	#################################################
	Armaments_Minister = {
		# Julius Hoppenrath
		DNZ_AM_Julius_Hoppenrath = {
			picture = Julius_Hoppenrath
			allowed = {
				tag = DNZ
			}
			available = {
				F_Minister_Allowed = yes
				NOT = {
					has_country_flag = Julius_Hoppenrath_unavailable
				}
			}
			visible = {
				date < 1961.4.19
			}
			traits = {
				ideology_F
				AM_Reformer
			}
		}
	}
}
