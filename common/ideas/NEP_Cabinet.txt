ideas = {
	#################################################
	### Head of Government
	#################################################
	Head_of_Government = {
		# Juddha Shumsher Jang Bahadur Rana
		NEP_D_Juddha_Shumsher_Jang_Bahadur_Rana = {
			picture = Juddha_Rana
			allowed = {
				tag = NEP
			}
			visible = {
				date < 1952.11.20
			}
			available = {
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Juddha_Shumsher_Jang_Bahadur_Rana_unavailable
				}
			}
			traits = {
				POSITION_Prime_Minister
				ideology_A
				L_Local_Tyrant
			}
		}
	}
	#################################################
	### Foreign Minister
	#################################################
	Foreign_Minister = {
		# Keshar Rana
		NEP_FM_Keshar_Rana = {
			picture = Keshar_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Keshar_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Biased_Intellectual
			}
		}
		# Dhir Rana
		NEP_FM_Dhir_Rana = {
			picture = Dhir_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Dhir_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Biased_Intellectual
			}
		}
		# Krishna Rana
		NEP_FM_Krishna_Rana = {
			picture = Krishna_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1940.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Krishna_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Apologetic_Clerk
			}
		}
		# Mohan Rana
		NEP_FM_Mohan_Rana = {
			picture = Mohan_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Mohan_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				FM_General_Staffer
			}
		}
		# Kiran Rana
		NEP_FM_Kiran_Rana = {
			picture = Kiran_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kiran_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				FM_Great_Compromiser
			}
		}
		# W.R. Parker-Gill
		NEP_FM_WR_ParkerGill = {
			picture = WR_ParkerGill
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = WR_ParkerGill_unavailable
				}
			}
			traits = {
				ideology_D
				FM_Iron_Fisted_Brute
			}
		}
		# William Stevenson Meyer
		NEP_FM_William_Stevenson_Meyer = {
			picture = William_Stevenson_Meyer
			allowed = {
				tag = NEP
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = William_Stevenson_Meyer_unavailable
				}
			}
			traits = {
				ideology_D
				FM_Ideological_Crusader
			}
		}
		# Edward Cook
		NEP_FM_Edward_Cook = {
			picture = Edward_Cook
			allowed = {
				tag = NEP
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Edward_Cook_unavailable
				}
			}
			traits = {
				ideology_D
				FM_The_Cloak_N_Dagger_Schemer
			}
		}
	}
	#################################################
	### Minister of Security
	#################################################
	Minister_of_Security = {
		# Juddha Rana
		NEP_MoS_Juddha_Rana = {
			picture = Juddha_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Juddha_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Silent_Lawyer
			}
		}
		# Mohan Rana
		NEP_MoS_Mohan_Rana = {
			picture = Mohan_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Mohan_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Silent_Lawyer
			}
		}
		# Padma Rana
		NEP_MoS_Padma_Rana = {
			picture = Padma_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Padma_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				MoS_Compassionate_Gentleman
			}
		}
		# Edward Cook
		NEP_MoS_Edward_Cook = {
			picture = Edward_Cook
			allowed = {
				tag = NEP
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Edward_Cook_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Efficient_Sociopath
			}
		}
		# H.T.S Collins
		NEP_MoS_HTS_Collins = {
			picture = HTS_Collins
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = HTS_Collins_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Prince_Of_Terror
			}
		}
		# W.R. Parker-Gill
		NEP_MoS_WR_ParkerGill = {
			picture = WR_ParkerGill
			allowed = {
				tag = NEP
			}
			available = {
				date > 1939.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = WR_ParkerGill_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Back_Stabber
			}
		}
		# Sidney R. Planter
		NEP_MoS_Sidney_R_Planter = {
			picture = Sidney_R_Planter
			allowed = {
				tag = NEP
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sidney_R_Planter_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Man_Of_The_People
			}
		}
		# William Stevenson Meyer
		NEP_MoS_William_Stevenson_Meyer = {
			picture = William_Stevenson_Meyer
			allowed = {
				tag = NEP
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = William_Stevenson_Meyer_unavailable
				}
			}
			traits = {
				ideology_D
				MoS_Crime_Fighter
			}
		}
	}
	#################################################
	### Armaments Minister
	#################################################
	Armaments_Minister = {
		# Mohan Rana
		NEP_AM_Mohan_Rana = {
			picture = Mohan_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Mohan_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Administrative_Genius
			}
		}
		# Gehendra Rana
		NEP_AM_Gehendra_Rana = {
			picture = Gehendra_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gehendra_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Air_Superiority_Proponent
			}
		}
		# Dhir Rana
		NEP_AM_Dhir_Rana = {
			picture = Dhir_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1939.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Dhir_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Theoretical_Scientist
			}
		}
		# Padma Rana
		NEP_AM_Padma_Rana = {
			picture = Padma_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Padma_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Military_Entrepreneur
			}
		}
		# Kiran Rana
		NEP_AM_Kiran_Rana = {
			picture = Kiran_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kiran_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				AM_Air_To_Ground_Proponent
			}
		}
		# William Stevenson Meyer
		NEP_AM_William_Stevenson_Meyer = {
			picture = William_Stevenson_Meyer
			allowed = {
				tag = NEP
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = William_Stevenson_Meyer_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Administrative_Genius
			}
		}
		# H.T.S Collins
		NEP_AM_HTS_Collins = {
			picture = HTS_Collins
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = HTS_Collins_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Submarine_Proponent
			}
		}
		# W.R. Parker-Gill
		NEP_AM_WR_ParkerGill = {
			picture = WR_ParkerGill
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = WR_ParkerGill_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Laissez-faire_Capitalist
			}
		}
		# Sidney R. Planter
		NEP_AM_Sidney_R_Planter = {
			picture = Sidney_R_Planter
			allowed = {
				tag = NEP
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sidney_R_Planter_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Resource_Industrialist
			}
		}
		# Robert Mainwaring
		NEP_AM_Robert_Mainwaring = {
			picture = Robert_Mainwaring
			allowed = {
				tag = NEP
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Robert_Mainwaring_unavailable
				}
			}
			traits = {
				ideology_D
				AM_Tank_Proponent
			}
		}
	}
	#################################################
	### Head of Intelligence
	#################################################
	Head_of_Intelligence = {
		# Baber Rana
		NEP_HoI_Baber_Rana = {
			picture = Baber_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Baber_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Industrial_Specialist
			}
		}
		# Mohan Rana
		NEP_HoI_Mohan_Rana = {
			picture = Mohan_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Mohan_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Dismal_Enigma
			}
		}
		# Dhir Rana
		NEP_HoI_Dhir_Rana = {
			picture = Dhir_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Dhir_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				HoI_Technical_Specialist
			}
		}
		# H.T.S Collins
		NEP_HoI_HTS_Collins = {
			picture = HTS_Collins
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = HTS_Collins_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Naval_Intelligence_Specialist
			}
		}
		# Sidney R. Planter
		NEP_HoI_Sidney_R_Planter = {
			picture = Sidney_R_Planter
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Sidney_R_Planter_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Industrial_Specialist
			}
		}
		# Robert Mainwaring
		NEP_HoI_Robert_Mainwaring = {
			picture = Robert_Mainwaring
			allowed = {
				tag = NEP
			}
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Robert_Mainwaring_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Logistics_Specialist
			}
		}
		# Edward Cook
		NEP_HoI_Edward_Cook = {
			picture = Edward_Cook
			allowed = {
				tag = NEP
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Edward_Cook_unavailable
				}
			}
			traits = {
				ideology_D
				HoI_Political_Specialist
			}
		}
	}
	#################################################
	### Chief of Staff
	#################################################
	Chief_of_Staff = {
		# Juddha Rana
		NEP_CoStaff_Juddha_Rana = {
			picture = Juddha_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Juddha_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Psychology
			}
		}
		# Baber Rana
		NEP_CoStaff_Baber_Rana = {
			picture = Baber_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Baber_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Manoeuvre
			}
		}
		# Mohan Rana
		NEP_CoStaff_Mohan_Rana = {
			picture = Mohan_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1942.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Mohan_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Defence
			}
		}
		# Padma Rana
		NEP_CoStaff_Padma_Rana = {
			picture = Padma_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Padma_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Psychology
			}
		}
		# Pyar Jung Thapa
		NEP_CoStaff_Pyar_Jung_Thapa = {
			picture = Pyar_Jung_Thapa
			allowed = {
				tag = NEP
			}
			available = {
				date > 1946.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Pyar_Jung_Thapa_unavailable
				}
			}
			traits = {
				ideology_A
				CoStaff_School_Of_Mass_Combat
			}
		}
		# H.T.S Collins
		NEP_CoStaff_HTS_Collins = {
			picture = HTS_Collins
			allowed = {
				tag = NEP
			}
			available = {
				date > 1943.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = HTS_Collins_unavailable
				}
			}
			traits = {
				ideology_D
				CoStaff_School_Of_Fire_Support
			}
		}
		# L.M. Potter
		NEP_CoStaff_LM_Potter = {
			picture = LM_Potter
			allowed = {
				tag = NEP
			}
			available = {
				date > 1944.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = LM_Potter_unavailable
				}
			}
			traits = {
				ideology_D
				CoStaff_School_Of_Mass_Combat
			}
		}
	}
	#################################################
	### Chief of Army
	#################################################
	Chief_of_Army = {
		# Padma Rana
		NEP_CoArmy_Padma_Rana = {
			picture = Padma_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Padma_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Guns_And_Butter_Doctrine
			}
		}
		# Baber Rana
		NEP_CoArmy_Baber_Rana = {
			picture = Baber_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Baber_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Elastic_Defence_Doctrine
			}
		}
		# Mohan Rana
		NEP_CoArmy_Mohan_Rana = {
			picture = Mohan_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Mohan_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoArmy_Static_Defence_Doctrine
			}
		}
		# Robert Mainwaring
		NEP_CoArmy_Robert_Mainwaring = {
			picture = Robert_Mainwaring
			allowed = {
				tag = NEP
			}
			available = {
				date > 1940.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = Robert_Mainwaring_unavailable
				}
			}
			traits = {
				ideology_D
				CoArmy_Armoured_Spearhead_Doctrine
			}
		}
		# L.M. Potter
		NEP_CoArmy_LM_Potter = {
			picture = LM_Potter
			allowed = {
				tag = NEP
			}
			available = {
				date > 1941.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = LM_Potter_unavailable
				}
			}
			traits = {
				ideology_D
				CoArmy_Decisive_Battle_Doctrine
			}
		}
	}
	#################################################
	### Chief of Navy
	#################################################
	Chief_of_Navy = {
		# Rudra Rana
		NEP_CoNavy_Rudra_Rana = {
			picture = Rudra_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Rudra_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoNavy_Base_Control_Doctrine
			}
		}
		# H.T.S Collins
		NEP_CoNavy_HTS_Collins = {
			picture = HTS_Collins
			allowed = {
				tag = NEP
			}
			available = {
				date > 1938.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = HTS_Collins_unavailable
				}
			}
			traits = {
				ideology_D
				CoNavy_Indirect_Approach_Doctrine
			}
		}
	}
	#################################################
	### Chief of Airforce
	#################################################
	Chief_of_Airforce = {
		# Kiran Rana
		NEP_CoAir_Kiran_Rana = {
			picture = Kiran_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1933.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Kiran_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoAir_Army_Aviation_Doctrine
			}
		}
		# Gehendra Rana
		NEP_CoAir_Gehendra_Rana = {
			picture = Gehendra_Rana
			allowed = {
				tag = NEP
			}
			available = {
				date > 1945.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = {
					has_country_flag = Gehendra_Rana_unavailable
				}
			}
			traits = {
				ideology_A
				CoAir_Air_Superiority_Doctrine
			}
		}
		# H.T.S Collins
		NEP_CoAir_HTS_Collins = {
			picture = HTS_Collins
			allowed = {
				tag = NEP
			}
			available = {
				date > 1937.1.1
				date < 1964.1.1
				D_Minister_Allowed = yes
				NOT = {
					has_country_flag = HTS_Collins_unavailable
				}
			}
			traits = {
				ideology_D
				CoAir_Carpet_Bombing_Doctrine
			}
		}
	}
}
