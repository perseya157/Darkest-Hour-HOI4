ideas = {
	high_command = {
		YUN_liang_peng = {
			picture = generic_air_asia_1
			allowed = {
				original_tag = YUN
			}
			traits = {
				air_strategic_bombing_2
			}
			ai_will_do = {
				factor = 1
			}
		}
		YUN_hu_wei = {
			picture = generic_army_asia_5
			allowed = {
				original_tag = YUN
			}
			traits = {
				army_infantry_2
			}
			ai_will_do = {
				factor = 1
			}
		}
		YUN_luo_sun = {
			picture = generic_army_asia_3
			allowed = {
				original_tag = YUN
			}
			traits = {
				army_artillery_2
			}
			ai_will_do = {
				factor = 1
			}
		}
		YUN_wang_jiang = {
			picture = generic_army_asia_2
			allowed = {
				original_tag = YUN
			}
			traits = {
				army_logistics_2
			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
