##################################################
### US AI Strategies
##################################################
#########################
# Diplomacy
#########################
# US-German Relations
US_German_Relations = {
	enable = {
		original_tag = USA
		USA = {
			has_government = democratic
		}
		GER = {
			has_government = fascist
		}
	}
	abort = {
		GER = {
			is_fascist = no
		}
	}
	ai_strategy = {
		type = antagonize
		id = "GER"
		value = -100
	}
	ai_strategy = {
		type = befriend
		id = "GER"
		value = -75
	}
	ai_strategy = {
		type = alliance
		id = "GER"
		value = -200
	}
	ai_strategy = {
		type = ignore
		id = "GER"
		value = -200
	}
	ai_strategy = {
		type = invade
		id = "GER"
		value = 200
	}
	ai_strategy = {
		type = conquer
		id = "GER"
		value = 200
	}
	ai_strategy = {
		type = contain
		id = "GER"
		value = 200
	}
}
# USA-Italian Relations
US_Italian_Relations = {
	enable = {
		original_tag = USA
		USA = {
			has_government = democratic
		}
		ITA = {
			has_government = fascist
		}
	}
	abort = {
		ITA = {
			is_fascist = no
		}
	}
	ai_strategy = {
		type = antagonize
		id = "ITA"
		value = -100
	}
	ai_strategy = {
		type = befriend
		id = "ITA"
		value = -75
	}
	ai_strategy = {
		type = alliance
		id = "ITA"
		value = -200
	}
	ai_strategy = {
		type = ignore
		id = "ITA"
		value = -200
	}
	ai_strategy = {
		type = invade
		id = "ITA"
		value = 200
	}
	ai_strategy = {
		type = conquer
		id = "ITA"
		value = 200
	}
	ai_strategy = {
		type = contain
		id = "ITA"
		value = 200
	}
}
# USA-Japanese Relations
US_Japanese_Relations = {
	enable = {
		original_tag = USA
		JAP = {
			has_government = fascist
		}
		USA = {
			has_government = democratic
		}
	}
	abort = {
		JAP = {
			is_fascist = no
		}
	}
	ai_strategy = {
		type = befriend
		id = "JAP"
		value = -50
	}
	ai_strategy = {
		type = alliance
		id = "JAP"
		value = -100
	}
	ai_strategy = {
		type = antagonize
		id = "JAP"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "JAP"
		value = 100
	}
	ai_strategy = {
		type = ignore
		id = "JAP"
		value = -200
	}
	ai_strategy = {
		type = invade
		id = "JAP"
		value = 150
	}
	ai_strategy = {
		type = conquer
		id = "JAP"
		value = 200
	}
}
# USA-French Relations
US_French_Relations = {
	enable = {
		original_tag = USA
		USA = {
			has_government = democratic
		}
		FRA = {
			has_government = democratic
		}
	}
	abort = {
		FRA = {
			NOT = { has_government = democratic }
		}
	}
	ai_strategy = {
		type = befriend
		id = "FRA"
		value = 50
	}
	ai_strategy = {
		type = alliance
		id = "FRA"
		value = 150
	}
	ai_strategy = {
		type = ignore
		id = "FRA"
		value = 200
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "FRA"
		value = 50
	}
	ai_strategy = {
		type = support
		id = "FRA"
		value = 200
	}
	ai_strategy = {
		type = protect
		id = "FRA"
		value = 200
	}
}
# US-British Relations
US_British_Relations = {
	enable = {
		original_tag = USA
		USA = {
			has_government = democratic
		}
		ENG = {
			has_government = democratic
		}
	}
	abort = {
		ENG = {
			NOT = { has_government = democratic }
		}
	}
	ai_strategy = {
		type = befriend
		id = "ENG"
		value = 50
	}
	ai_strategy = {
		type = alliance
		id = "ENG"
		value = 150
	}
	ai_strategy = {
		type = ignore
		id = "ENG"
		value = 200
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "ENG"
		value = 150
	}
	ai_strategy = {
		type = support
		id = "ENG"
		value = 200
	}
	ai_strategy = {
		type = protect
		id = "ENG"
		value = 200
	}
}
# US-Soviet Relations
US_Soviet_Relations = {
	enable = {
		original_tag = USA
		USA = {
			has_government = democratic
		}
		SOV = {
			is_fascist = no
		}
	}
	abort = {
		SOV = {
			has_government = fascist
		}
	}
	ai_strategy = {
		type = befriend
		id = "SOV"
		value = 50
	}
	ai_strategy = {
		type = alliance
		id = "SOV"
		value = -150
	}
	ai_strategy = {
		type = ignore
		id = "SOV"
		value = 200
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "SOV"
		value = -50
	}
	ai_strategy = {
		type = support
		id = "SOV"
		value = 200
	}
	ai_strategy = {
		type = protect
		id = "SOV"
		value = 75
	}
}
USA_save_the_aussies = {
	enable = {
		tag = USA
		has_government = democratic
		is_in_faction = no
		has_war = no
		JAP = {
			has_war_with = AST
		}
		AST = {
			has_government = democratic
			surrender_progress > 0.15
		}
		has_opinion = {
			target = AST
			value > 0			# not if we hate AST
		}
	}
	abort = {
		OR = {
			has_war_with = AST
			is_in_faction_with = JAP
			JAP = {
				NOT = {
					has_war_with = AST
				}
			}
		}
	}
	ai_strategy = {
		type = contain
		id = "JAP"
		value = 200
	}
	ai_strategy = {
		type = protect
		id = "AST"
		value = 200
	}
}
USA_save_india = {
	# in the case where japan has beaten china and is moving on india we should step up
	enable = {
		tag = USA
		has_government = democratic
		is_in_faction = no
		has_war = no
		JAP = {
			has_war_with = RAJ
			NOT = {
				has_war_with = CHI
			}
		}
		RAJ = {
			is_in_faction_with = ENG
			surrender_progress > 0.15
		}
	}
	abort = {
		OR = {
			has_war_with = RAJ
			is_in_faction_with = JAP
			JAP = {
				NOT = {
					has_war_with = RAJ
				}
			}
		}
	}
	ai_strategy = {
		type = contain
		id = "JAP"
		value = 200
	}
	ai_strategy = {
		type = protect
		id = "RAJ"
		value = 200
	}
}
USA_stop_soviet_from_falling = {
	enable = {
		tag = USA
		has_war = no
		num_divisions > 85		# we must have some divisions ourselves
		NOT = {
			has_government = fascist
		}
		NOT = {
			is_in_faction_with = GER
		}
		NOT = {
			has_war_with = SOV
		}
		GER = {
			has_war_with = SOV
			has_war_with = ENG
		}
		SOV = {
			surrender_progress > 0.05
			has_capitulated = no
		}
	}
	abort = {
		OR = {
			has_war = yes
			has_war_with = SOV
			NOT = {
				GER = {
					has_war_with = SOV
				}
			}
			is_in_faction_with = GER
			has_government = fascist
			SOV = {
				has_war = no
			}
			SOV = {
				surrender_progress < 0.01
			}
		}
	}
	ai_strategy = {
		type = support
		id = "SOV"
		value = 100
	}
	ai_strategy = {
		type = role_ratio
		id = infantry
		value = -75
	}
	ai_strategy = {
		type = template_prio
		id = light_armor
		value = -25
	}
	ai_strategy = {
		type = template_prio
		id = medium_armor
		value = -25
	}
	ai_strategy = {
		type = template_prio
		id = heavy_armor
		value = -25
	}
}
#########################
# Military
#########################
## Help Australia in case they're getting invaded
USA_help_AST = {
	enable = {
		tag = USA
		USA = {
			has_government = democratic
		}
		is_in_faction = no
		has_war = no
		JAP = {
			has_war_with = AST
		}
		AST = {
			has_government = democratic
			surrender_progress > 0.15
		}
		has_opinion = {
			target = AST
			value > 0
		}
	}
	abort = {
		OR = {
			has_war_with = AST
			is_in_faction_with = JAP
			JAP = {
				NOT = {
					has_war_with = AST
				}
			}
		}
	}
	ai_strategy = {
		type = support
		id = "AST"
		value = 100
	}
	ai_strategy = {
		type = protect
		id = "AST"
		value = 200
	}
	ai_strategy = {
		type = contain
		id = "JAP"
		value = 200
	}
}
## Help British Raj in case they're getting invaded
USA_help_RAJ = {
	enable = {
		tag = USA
		USA = {
			has_government = democratic
		}
		is_in_faction = no
		has_war = no
		JAP = {
			has_war_with = AST
		}
		RAJ = {
			has_government = democratic
			surrender_progress > 0.15
		}
		has_opinion = {
			target = RAJ
			value > 0
		}
	}
	abort = {
		OR = {
			has_war_with = RAJ
			is_in_faction_with = JAP
			JAP = {
				NOT = {
					has_war_with = RAJ
				}
			}
		}
	}
	ai_strategy = {
		type = support
		id = "RAJ"
		value = 100
	}
	ai_strategy = {
		type = protect
		id = "RAJ"
		value = 200
	}
	ai_strategy = {
		type = contain
		id = "JAP"
		value = 200
	}
}
## Help the Soviet Union (Open up a second Front)
USA_help_Soviet = {
	enable = {
		tag = USA
		USA = {
			has_government = democratic
		}
		is_in_faction = no
		has_war = no
		GER = {
			has_war_with = SOV
		}
		SOV = {
			NOT = {
				has_government = fascist
			}
			surrender_progress > 0.15
		}
		has_opinion = {
			target = SOV
			value > 0
		}
	}
	abort = {
		OR = {
			has_war_with = SOV
			is_in_faction_with = GER
			GER = {
				NOT = {
					has_war_with = SOV
				}
			}
		}
	}
	ai_strategy = {
		type = support
		id = "SOV"
		value = 100
	}
	ai_strategy = {
		type = contain
		id = "GER"
		value = 200
	}
}
# this one is same as above, but if you have entered the war yourself
USA_stop_soviet_from_falling_2 = {
	enable = {
		tag = USA
		has_war = yes
		NOT = {
			has_government = fascist
		}
		NOT = {
			is_in_faction_with = GER
		}
		NOT = {
			has_war_with = SOV
		}
		GER = {
			has_war_with = SOV
			has_war_with = ENG
		}
		SOV = {
			surrender_progress > 0.05
			has_capitulated = no
		}
	}
	abort = {
		OR = {
			has_war = no
			has_war_with = SOV
			NOT = {
				GER = {
					has_war_with = SOV
				}
			}
			is_in_faction_with = GER
			has_government = fascist
			SOV = {
				has_war = no
			}
			SOV = {
				surrender_progress < 0.01
			}
		}
	}
	ai_strategy = {
		type = support
		id = "SOV"
		value = 100
	}
}
#########################
# Region Priority
#########################
## US Region Priorities
US_area_priority = {
	enable = {
		original_tag = USA
	}
	ai_strategy = {
		type = area_priority
		id = europe
		value = 100
	}
	ai_strategy = {
		type = area_priority
		id = north_america
		value = 150
	}
	ai_strategy = {
		type = area_priority
		id = south_america
		value = 25
	}
	ai_strategy = {
		type = area_priority
		id = asia
		value = 10
	}
	ai_strategy = {
		type = area_priority
		id = pacific
		value = 200
	}
	ai_strategy = {
		type = area_priority
		id = oceania
		value = 150
	}
	ai_strategy = {
		type = area_priority
		id = middle_east
		value = 80
	}
	ai_strategy = {
		type = area_priority
		id = africa
		value = 75
	}
}
#########################
# Production & Templates
#########################
USA_template_design_1 = {
	enable = {
		original_tag = USA
		date < 1940.1.1
	}
	abort = {
		date > 1940.1.1
	}
	ai_strategy = {
		type = template_prio
		id = motorized
		value = 10
	}
	ai_strategy = {
		type = template_prio
		id = cavalry
		value = -100
	}
	ai_strategy = {
		type = template_prio
		id = light_armor
		value = 50
	}
	ai_strategy = {
		type = template_prio
		id = medium_armor
		value = -10
	}
	ai_strategy = {
		type = template_prio
		id = heavy_armor
		value = -50
	}
	ai_strategy = {
		type = template_prio
		id = paratroopers
		value = -50
	}
	ai_strategy = {
		type = template_prio
		id = infantry
		value = 60
	}
}
USA_template_design_2 = {
	enable = {
		original_tag = USA
		date > 1940.1.1
	}
	ai_strategy = {
		type = template_prio
		id = mechanized
		value = -20
	}
	ai_strategy = {
		type = template_prio
		id = cavalry
		value = -100
	}
	ai_strategy = {
		type = template_prio
		id = light_armor
		value = 10
	}
	ai_strategy = {
		type = template_prio
		id = medium_armor
		value = 20
	}
	ai_strategy = {
		type = template_prio
		id = modern_armor
		value = 40
	}
	ai_strategy = {
		type = template_prio
		id = heavy_armor
		value = -20
	}
	ai_strategy = {
		type = template_prio
		id = paratroopers
		value = -50
	}
	ai_strategy = {
		type = template_prio
		id = infantry
		value = 10
	}
	ai_strategy = {
		type = template_prio
		id = marines
		value = 100
	}
}
USA_garrison_production = {
	enable = {
		original_tag = USA
		ai_wants_divisions > 12		#Massively decreasing the amount of paratroopers the AI wants
	}
	abort = {
		ai_wants_divisions < 13		#Massively decreasing the amount of paratroopers the AI wants
	}
	ai_strategy = {
		type = role_ratio
		id = garrison
		value = 3
	}
}
