######################################################################
# Darkest Hour Events : Soviet-German War
######################################################################
add_namespace = GER_SOV_War
###########################
# Barbarossa Thwarted; New Directives Required
###########################
country_event = {
	id = GER_SOV_War.0
	title = GER_SOV_War.0.t
	desc = GER_SOV_War.0.d
	picture = GFX_news_event_polish_tanks
	is_triggered_only = yes
	# Advance in the North
	option = {
		name = GER_SOV_War.0.A
		unlock_decision_tooltip = GER_Unternehmen_Hirsch
		set_country_flag = GER_Advance_in_the_North_Second_Year
	}
	# Advance in the Centre
	option = {
		name = GER_SOV_War.0.B
		unlock_decision_tooltip = GER_Fall_Kreml
		set_country_flag = GER_Advance_in_the_Centre_Second_Year
	}
	# Advance in the South 
	option = {
		name = GER_SOV_War.0.C
		unlock_decision_tooltip = GER_Fall_Blau
		set_country_flag = GER_Advance_in_the_South_Second_Year
	}
}
###########################
# German Betrayal
###########################
country_event = {
	id = GER_SOV_War.1
	title = GER_SOV_War.1.t
	desc = GER_SOV_War.1.d
	picture = GFX_report_event_generic_army
	fire_only_once = yes
	is_triggered_only = yes
	# 
	option = {
		name = GER_SOV_War.1.A
		add_ideas = SOV_The_German_Betrayal
	}
}
###########################
# The Great Eastern Crusade
###########################
country_event = {
	id = GER_SOV_War.2
	title = GER_SOV_War.2.t
	desc = GER_SOV_War.2.d
	picture = GFX_report_event_generic_army
	fire_only_once = yes
	is_triggered_only = yes
	# 
	option = {
		name = GER_SOV_War.2.A
		
	}
}
###########################
# Germany Commence Hostilities with the Soviet Union
###########################
news_event = {
	id = GER_SOV_War.3
	title = GER_SOV_War.3.t
	desc = GER_SOV_War.3.d
	picture = GFX_report

	major = yes

	is_triggered_only = yes
	# 
	option = {
		name = GER_SOV_War.3.A
		
	}
}
###########################
# Stalin flees to his Dacha
###########################
country_event = {
	id = GER_SOV_War.4
	title = GER_SOV_War.4.t
	desc = GER_SOV_War.4.d
	picture = GFX_report_event_generic_army
	fire_only_once = yes
	trigger = {
		original_tag = SOV
		SOV = { has_war_with = GER }
		OR = {
			NOT = { controls_state = 195 }
			NOT = { controls_state = 219 }
			NOT = { controls_state = 206 }
			NOT = { controls_state = 202 }
			NOT = { controls_state = 218 }
			NOT = { controls_state = 137 }
			NOT = { controls_state = 217 }
			NOT = { controls_state = 1352 }
			NOT = { controls_state = 229 }
		}
	}
	# 
	option = {
		name = GER_SOV_War.4.A
		add_country_leader_trait = L_Command_Structure_without_Commander
		hidden_effect = {
			country_event = { id = GER_SOV_War.5 days = 3 }
		}
	}
}
###########################
# Party Members Meet Stalin
###########################
country_event = {
	id = GER_SOV_War.5
	title = GER_SOV_War.5.t
	desc = GER_SOV_War.5.d
	picture = GFX_report_event_generic_army
	fire_only_once = yes
	is_triggered_only = yes
	# 
	option = {
		name = GER_SOV_War.5.A
		remove_country_leader_trait = L_Command_Structure_without_Commander
		hidden_effect = {
			country_event = { id = GER_SOV_War.6 days = 5 }
		}
	}
}
###########################
# The Great Patriotic War
###########################
country_event = {
	id = GER_SOV_War.6
	title = GER_SOV_War.6.t
	desc = GER_SOV_War.6.d
	picture = GFX_report_event_generic_army
	fire_only_once = yes
	is_triggered_only = yes
	# 
	option = {
		name = GER_SOV_War.6.A
		add_political_power = 100
		swap_ideas = {
			add_idea = SOV_The_German_Betrayal_2
			remove_idea = SOV_The_German_Betrayal
		}
		set_country_flag = SOV_The_Great_Patriotic_War
		unlock_decision_category_tooltip = SOV_The_Great_Patriotic_War_category
	}
}
###########################
# Red Star Rising
###########################
country_event = {
	id = GER_SOV_War.7
	title = GER_SOV_War.7.t
	desc = GER_SOV_War.7.d
	picture = GFX_report_event_generic_army
	fire_only_once = yes
	is_triggered_only = yes
	# 
	option = {
		name = GER_SOV_War.7.A
	}
}
###########################
# First Moscow Conference
###########################
news_event = {
	id = GER_SOV_War.8
	title = GER_SOV_War.8.t
	desc = GER_SOV_War.8.d
	picture = GFX_report_event_generic_army
	is_triggered_only = yes

	major = yes

	# 
	option = {
		name = GER_SOV_War.8.A
	}
}
###########################
# Destruction of the Dneprostroi Dam
###########################
news_event = {
	id = GER_SOV_War.9
	title = GER_SOV_War.9.t
	desc = GER_SOV_War.9.d
	picture = GFX_report_event_generic_army
	is_triggered_only = yes

	major = yes

	# 
	option = {
		name = GER_SOV_War.9.A
	}
}