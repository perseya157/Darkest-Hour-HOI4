﻿###########################
# Darkest Hour Surrender Events : Der Untergang
###########################

add_namespace = Surrender_ROM
add_namespace = Surrender_HUN
add_namespace = Surrender_SOV_Response

#########################################################################
# Surrender to the Soviets? (Romania)
#########################################################################
country_event = {
	id = Surrender_ROM.1
	title = Surrender_ROM.1.t
	desc = Surrender_ROM.1.d
	picture = GFX_news_event_002
	fire_only_once = yes

	trigger = {
		SOV = { has_war_with = ROM }
		SOV = { has_war_with = GER }
		is_in_faction_with = GER 
		any_other_country = {
			OR = {
				tag = SOV
				is_in_faction_with = SOV
			}
			controls_state = 79
			controls_state = 78
		}
	}

	option = { #Surrender to the Soviets
		name = Surrender_ROM.1.A
		#kill_country_leader = yes 
		SOV = {
			country_event = { id = Surrender_SOV.10 }
		}
	}

	option = { #We Won't surrender
		name = Surrender_ROM.1.B
	}
}

#########################################################################
# Soviet answer to Romanian surrender
#########################################################################
country_event = {
	id = Surrender_SOV_Response.1
	title = Surrender_SOV_Response.1.t
	desc = Surrender_SOV_Response.1.d
	picture = GFX_news_event_002
	fire_only_once = yes

	is_triggered_only = yes

	#Puppet Romania
	option = { 
		name = Surrender_SOV_Response.1.A
		ROM = { white_peace = SOV }

		hidden_effect = {
			if = {
				limit = {
					OR = {
						has_dlc = "Together for Victory"
						has_dlc = "Death or Dishonor"
					}
				}
				set_autonomy = {
					target = ROM
					autonomous_state = AUTONOMY_Puppet
				}
				else = {
					puppet = ROM
				}
			}
			SOV = { #In case the Soviets hadn't recieved Bessarabia beforehand
				transfer_state = 78
				transfer_state = 80
				transfer_state = 835
				transfer_state = 836
				transfer_state = 1145
				transfer_state = 1143
			}
		}
	}

	#No compromise with the fascists - keep fighting
	option = { 
		name = Surrender_SOV_Response.1.B
	}
}


### -------------------  ---------------------------------------------

#########################################################################
# Surrender to the Soviets? (Hungary)
#########################################################################
country_event = {
	id = Surrender_HUN.1
	title = Surrender_HUN.1.t
	desc = Surrender_HUN.1.d
	picture = GFX_news_event_002
	fire_only_once = yes

	trigger = {
		SOV = { has_war_with = HUN }
		SOV = { has_war_with = GER }
		is_in_faction_with = GER 
		any_other_country = {
			OR = {
				tag = SOV
				is_in_faction_with = SOV
			}
			controls_state = 154
		}
	}

	#Surrender to the Soviets
	option = { 
		name = Surrender_HUN.1.A
		SOV = {
			country_event = { id = Surrender_SOV.11 }
		}
	}

	#We Won't surrender
	option = { 
		name = Surrender_HUN.1.B
	}
}

#########################################################################
# Soviet answer to Hungarian surrender
#########################################################################
country_event = {
	id = Surrender_SOV_Response.2
	title = Surrender_SOV_Response.2.t
	desc = Surrender_SOV_Response.2.d
	picture = GFX_news_event_002
	fire_only_once = yes

	is_triggered_only = yes

	#Puppet Hungary
	option = { 
		name = Surrender_SOV_Response.11.A
		HUN = { white_peace = SOV }

		hidden_effect = {
			if = {
				limit = {
					OR = {
						has_dlc = "Together for Victory"
						has_dlc = "Death or Dishonor"
					}
				}
				set_autonomy = {
					target = HUN
					autonomous_state = AUTONOMY_Puppet
				}
				else = {
					puppet = HUN
				}
			}
			
			SOV = {
				transfer_state = 73 # Transfer of Ruthenian Carpathia
			}
		}
	}	

	#No compromise with the fascists - keep fighting
	option = { 
		name = Surrender_SOV_Response.2.B
	}
}
