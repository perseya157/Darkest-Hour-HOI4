﻿#########################################################################
# Darkest Hour Events : Assassination of Kirov
#########################################################################
add_namespace = SOV_Kirov_Question
#########################################################################
# Kirov Refused our invitation
#########################################################################
country_event = {
	id = SOV_Kirov_Question.1
	title = SOV_Kirov_Question.1.t
	desc = SOV_Kirov_Question.1.d
	picture = GFX_report_SOV_Kirov_2
	fire_only_once = yes
	is_triggered_only = yes
	# So this snake doesn't respect the man who give him everything
	option = {
		name = SOV_Kirov_Question.1.A
		add_political_power = -10
		set_country_flag = kirov_in_leningrad
	}
}
#########################################################################
# Yagoda/Agranov Accepts to replace the Leningrad NKVD Leadership
#########################################################################
country_event = {
	id = SOV_Kirov_Question.2
	title = SOV_Kirov_Question.2.t
	desc = SOV_Kirov_Question.2.d
	picture = GFX_report_SOV_Yagoda
	fire_only_once = yes
	is_triggered_only = yes
	# Perfect
	option = {
		name = SOV_Kirov_Question.2.A
		country_event = {
			id = SOV_Kirov_Question.3
			days = 2
		}
	}
}
#########################################################################
# Kirov interfered.
#########################################################################
country_event = {
	id = SOV_Kirov_Question.3
	title = SOV_Kirov_Question.3.t
	desc = SOV_Kirov_Question.3.d
	picture = GFX_report_SOV_Kirov
	fire_only_once = yes
	is_triggered_only = yes
	# So Kirov thinks that he could do that?
	option = {
		name = SOV_Kirov_Question.3.A
		add_political_power = -25
		set_country_flag = kirov_interference
	}
}
#########################################################################
# Yagoda/Agranov presents his suggestion
#########################################################################
country_event = {
	id = SOV_Kirov_Question.4
	title = SOV_Kirov_Question.4.t
	desc = SOV_Kirov_Question.4.d
	picture = GFX_report_SOV_Yagoda_and_Agranov
	fire_only_once = yes
	is_triggered_only = yes
	# We do not need to get your hands dirty,
	option = {
		name = SOV_Kirov_Question.4.A
		add_political_power = 5
		custom_effect_tooltip = SOV_Kirov_Question.4.A.tt
		set_country_flag = independent_nikolayev
	}
	# Find Nikolayev and order him to assassinate Kirov
	option = {
		name = SOV_Kirov_Question.4.B
		country_event = {
			id = SOV_Kirov_Question.5
			days = 2
		}
	}
}
#########################################################################
# Nikolayev Accepts
#########################################################################
country_event = {
	id = SOV_Kirov_Question.5
	title = SOV_Kirov_Question.5.t
	desc = SOV_Kirov_Question.5.d
	picture = GFX_report_SOV_Nikolayev
	fire_only_once = yes
	is_triggered_only = yes
	# Good boy...
	option = {
		name = SOV_Kirov_Question.5.A
		news_event = {
			id = SOV_Kirov_Question.6
			days = 7
		}
	}
}
#########################################################################
# Sergei Kirov Assassinated in Suspicious Circunstances
#########################################################################
news_event = {
	id = SOV_Kirov_Question.6
	title = SOV_Kirov_Question.6.t
	desc = SOV_Kirov_Question.6.d
	picture = GFX_news_SOV_Kirov_Death
	fire_only_once = yes
	is_triggered_only = yes
	# We do not need to get your hands dirty,
	option = {
		name = SOV_Kirov_Question.6.A
		trigger = {
			original_tag = SOV
		}
		unlock_national_focus = SOV_The_Kirov_Assassination
		# remove_mission = SOV_Resolve_the_Kirov_Question
		set_country_flag = Sergey_Kirov_unavailable
		add_stability = -0.10
	}
	# We do not need to get your hands dirty,
	option = {
		name = SOV_Kirov_Question.6.B
		trigger = {
			NOT = {
				original_tag = SOV
			}
		}
	}
}
#########################################################################
# Nikolayev arrested in a probable assassination attempt
#########################################################################
country_event = {
	id = SOV_Kirov_Question.7
	title = SOV_Kirov_Question.7.t
	desc = SOV_Kirov_Question.7.d
	picture = GFX_report_SOV_Nikolayev
	fire_only_once = yes
	trigger = {
		original_tag = SOV
		date > 1934.10.15
		has_start_date < 1934.10.15
		has_country_flag = independent_nikolayev
	}
	# Release him with his weapon
	option = {
		name = SOV_Kirov_Question.7.A
		add_political_power = 25
		clr_country_flag = independent_nikolayev
		set_country_flag = kirov_assassination_attempt
	}
	# Traitor! Sentence him for forced labour
	option = {
		name = SOV_Kirov_Question.7.A
		add_political_power = -50
		clr_country_flag = independent_nikolayev
		set_country_flag = kirov_path
	}
}
#########################################################################
# Kiroff, High Soviet Leader, Slain
#########################################################################
news_event = {
	id = SOV_Kirov_Question.8
	title = SOV_Kirov_Question.8.t
	desc = SOV_Kirov_Question.8.d
	picture = GFX_news_SOV_Kirov_Death
	fire_only_once = yes
	trigger = {
		SOV = {
			has_country_flag = SOV_Unassign_Kirovs_Bodyguards
			has_country_flag = SOV_Unassign_The_Smolny_Institute_Guard_Post
		}
		date > 1934.12.1
		has_start_date < 1934.12.1
	}
	# We do not need to get your hands dirty,
	option = {
		name = SOV_Kirov_Question.8.A
		trigger = {
			original_tag = SOV
		}
		unlock_national_focus = SOV_The_Kirov_Assassination
		# remove_mission = SOV_Resolve_the_Kirov_Question
		set_country_flag = Sergey_Kirov_unavailable
		add_political_power = 50
	}
	# We do not need to get your hands dirty,
	option = {
		name = SOV_Kirov_Question.8.B
		trigger = {
			NOT = {
				original_tag = SOV
			}
		}
	}
}