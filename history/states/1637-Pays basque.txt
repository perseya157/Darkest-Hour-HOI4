state={
	id=1637
	name="STATE_1637"

	history={
		owner = FRA
		victory_points = {
			743 1 
		}
		buildings = {
			infrastructure = 4

		}
		add_core_of = FRA
		add_core_of = BSQ
		1941.6.22 = {
			owner = FRA
			controller = GER
			add_core_of = VIC

		}
		1945.1.1 = {
			owner = FRA
			controller = FRA

		}
		1946.1.1 = {
			remove_core_of = VIC

		}

	}

	provinces={
		743 13517 
	}
	manpower=30290
	buildings_max_level_factor=1.000
	state_category=rural
}
