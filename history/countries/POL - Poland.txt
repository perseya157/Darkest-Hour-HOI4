﻿######################################################################
# Poland - 1933
######################################################################
1933.1.1 = {
	capital = 10
	oob = "POL_1933"
	set_research_slots = 3
	set_convoys = 10
	add_manpower = 200000
	add_to_variable = { money = 160 }
	#######################
	# Research
	#######################
	set_technology = {
		### Infantry Tech
		Small_Arms_1916 = 1
		Small_Arms_1900 = 1
		Infantry_Kit_1914 = 1
		Infantry_Kit_1918 = 1

		Heavy_Weapons_1905 = 1
		Heavy_Weapons_1916 = 1
		### Support Tech
		tech_mountaineers = 1
		### Artillery Tech
		Artillery_1910 = 1
		Anti_Aircraft_Gun_1914 = 1
		Artillery_Range_Finding_and_Surveying_Tools = 1
		Infantry_Support_Gun_1914 = 1
		Artillery_1916 = 1
		High_Explosive_Shells = 1
		### Armour Tech
		Motorized_1916 = 1
		Armored_Car_1911 = 1
		Armored_Car_1916 = 1
		Armored_Car_1926 = 1
		Heavy_Tank_1916 = 1
		Heavy_Tank_1917 = 1
		Super_Heavy_Tank_1917 = 1
		Light_Tank_1917 = 1
		Light_Tank_1919 = 1
		Light_Tank_1926 = 1
		Medium_Tank_1926 = 1
		### Air Tech
		Unarmed_Recon_1910 = 1
		Fighter_1914 = 1
		Fighter_1916 = 1
		Fighter_1918 = 1
		Fighter_1924 = 1
		Fighter_Bomber_1916 = 1
		Fighter_Bomber_1918 = 1
		Fighter_Bomber_1924 = 1
		Tactical_Bomber_1914 = 1
		Tactical_Bomber_1916 = 1
		Tactical_Bomber_1918 = 1
		Tactical_Bomber_1925 = 1
		Strategic_Bomber_1916 = 1
		#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		CA_1885 = 1
		CA_1895 = 1
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
		### Land Doctrines
		Twentieth_Century_Warfare = 1
		Leading_by_Order = 1
		Strongpoint = 1
		Mass_Charge = 1
		Static_Defence = 1
		Counterattack = 1
		Defence_In_Depth = 1
		Separate_Arms_Force = 1
		Combined_Arms_Effort = 1
		Specialized_Branch_Tactical_Experience = 1
		Beyond_Trench_Warfare = 1
		### Engineering
		computing_1 = 1
		computing_2 = 1
		computing_3 = 1
		decryption_encryption_1 = 1
		### Support Companies
		tech_engineers = 1
		tech_assault_weapons_1 = 1
		tech_assault_weapons_2 = 1
		tech_assault_weapons_3 = 1
		tech_entrenchment_1 = 1
		tech_entrenchment_2 = 1
		tech_entrenchment_3 = 1
		tech_amphibious_1 = 1
		tech_amphibious_2 = 1
		tech_amphibious_3 = 1
		tech_recon = 1
		tech_recon2 = 1
		tech_recon3 = 1
		tech_recon4 = 1
		tech_military_police = 1
		tech_military_police2 = 1
		tech_military_police3 = 1
		tech_military_police4 = 1
		tech_signal_company = 1
		tech_signal_company2 = 1
		tech_field_hospital = 1
		tech_field_hospital_psychology_1 = 1
		tech_field_hospital_psychology_2 = 1
		tech_field_hospital_psychology_3 = 1
		tech_field_hospital_health_1 = 1
		tech_field_hospital_health_2 = 1
		tech_field_hospital_health_3 = 1
		tech_logistics_company = 1
		tech_logistics_company2 = 1
		tech_logistics_company3 = 1
		tech_logistics_company4 = 1
		tech_maintenance_company = 1
		tech_maintenance_company2 = 1
		tech_maintenance_company3 = 1
		tech_maintenance_company4 = 1
	}
	#######################
	# Diplomacy
	#######################
	### Soviet–Polish Non-Aggression Pact
	diplomatic_relation = {
		country = SOV
		relation = non_aggression_pact
		active = yes
	}
	### Danzig Crisis Aftermath
	diplomatic_relation = {
		country = DNZ
		relation = docking_rights
		active = yes
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1930.11.23"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 4
		authoritarian = 53
		democratic = 19
		socialist = 14
		communist = 10
	}
	### Great Depression/Unemployment
	add_ideas = generic_unemployment_idea
	set_variable = { unemployment_value_modifier = -0.2 }
	set_variable = { current_unemployment = 0.44 }
	clamp_variable = {
		var = current_unemployment
		min = 0
		max = 1
	}
	update_unemployment_modifier = yes
	add_ideas = {
		# Spirit
		POL_May_Coup
		POL_Embers_of_Industry
		# Cabinet
		POL_D_Ignacy_Moscicki
		POL_FM_Jozef_Beck
		POL_AM_Jan_Pilsudski
		POL_MoS_Bronislaw_Piernacki
		# POL_HoI_Janusz_Jagrym_Maleszewski
		# Military Staff
		POL_CoStaff_Janusz_Gasiorowski
		POL_CoArmy_Jozef_Pilsudski
		POL_CoNavy_Jerzy_Swirski
		POL_CoAir_Ludomil_Rayski
		# Policies
		industrializing_economy
		limited_civil_liberties
		taxation_level_4
		social_spending_level_3
		research_spending_level_1
		army_spending_level_3
	}
	create_intelligence_agency = {
		name = "Dwójka"
		icon = "GFX_intelligence_agency_logo_pol"
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Jan Mosdorf"
		desc = ""
		picture = GFX_P_F_Jan_Mosdorf
		expire = "1965.1.1"
		ideology = falangism
		traits = { POSITION_President SUBIDEOLOGY_Falangism L_Barking_Buffoon }
	}
	# Paternal Autocracy
	create_country_leader = {
		name = "Ignacy Moscicki"
		desc = ""
		picture = GFX_P_A_Ignacy_Moscicki
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { POSITION_President SUBIDEOLOGY_Authoritarian_Democracy L_Stern_Imperialist }
	}
	create_country_leader = {
		name = "Józef Pilsudski"
		desc = ""
		picture = GFX_P_A_Josef_Pilsudski
		expire = "1935.5.12"
		ideology = authoritarian_democracy
		traits = { POSITION_Marshal SUBIDEOLOGY_Authoritarian_Democracy L_Resigned_Generalissimo }
	}
	# Democracy
	create_country_leader = {
		name = "Roman Dmowski"
		desc = ""
		picture = GFX_P_D_Roman_Dmowski
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Social_Conservatism L_Weary_Stiff_Neck }
	}
	# Socialism
	create_country_leader = {
		name = "Ignacy Daszynski"
		desc = ""
		picture = GFX_P_S_Ignacy_Daszynski
		expire = "1965.1.1"
		ideology = socialism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Socialism L_Popular_Figurehead }
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Boleslaw Bierut"
		desc = ""
		picture = GFX_P_C_Boleslaw_Bierut
		expire = "1965.1.1"
		ideology = stalinism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Stalinism L_Barking_Buffoon }
	}
	#######################
	# Generals
	#######################
	create_field_marshal = {
		name = "Józef Piłsudski"
	    id = 213000
		GFX = GFX_M_Josef_Pilsudski_MILITARY
		traits = { media_personality offensive_doctrine inspirational_leader }
		skill = 2
		attack_skill = 3
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Kazimierz Sosnkowski"
	    id = 213001
		GFX = GFX_M_Kazimierz_Sosnowski
		traits = { old_guard organizer }
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 4
	}
	create_corps_commander = {
		name = "Edward Rydz-Śmigły"
	    id = 213002
		GFX = GFX_M_Edward_Rydz_Smigly
		traits = {  politically_connected war_hero media_personality skilled_staffer  }
		skill = 2
		attack_skill = 3
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Leon Berbecki"
	    id = 213003
		GFX = GFX_M_Leon_Berbecki
		traits = {  old_guard infantry_officer politically_connected }
		skill = 2
		attack_skill = 2
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Jan Kruszewski"
	    id = 213004
		GFX = GFX_M_Jan_Kruszewski
		traits = {  politically_connected infantry_officer }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Juliusz Rómmel"
	    id = 213005
		GFX = GFX_M_Juliusz_Rommel
		traits = {  old_guard war_hero trait_cautious cavalry_officer cavalry_leader }
		skill = 2
		attack_skill = 4
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Tadeusz Piskor"
	    id = 213006
		GFX = GFX_M_Tadeusz_Piskor
		traits = {  infantry_officer trait_engineer }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Stefan Dab-Biernacki"
	    id = 213007
		GFX = GFX_M_Stefan_Dab_Biernacki
		traits = {  old_guard politically_connected harsh_leader substance_abuser }
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Gustaw Orlicz-Dreszer"
	    id = 213008
		GFX = GFX_M_Gustaw_Orlicz-Dreszer
		traits = {  politically_connected }
		skill = 1
		attack_skill = 2
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Antoni Szyling"
	    id = 213009
		GFX = GFX_M_Antoni_Szyling
		traits = { inflexible_strategist old_guard trickster }
		skill = 4
		attack_skill = 2
		defense_skill = 5
		planning_skill = 4
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Wiktor Thomée"
	    id = 213010
		GFX = GFX_M_Wiktor_Thomee
		traits = {  harsh_leader organizer commando }
		skill = 4
		attack_skill = 2
		defense_skill = 5
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Franciszek Kleeberg"
	    id = 213011
		GFX = GFX_M_Franciszek_Kleeberg
		traits = {  infantry_officer infantry_leader swamp_fox }
		skill = 3
		attack_skill = 3
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 4
	}
	create_corps_commander = {
		name = "Tadeusz Kutrzeba"
	    id = 213012
		GFX = GFX_M_Tadeusz_Kutrzeba
		traits = {  brilliant_strategist career_officer skilled_staffer trait_engineer }
		skill = 3
		attack_skill = 2
		defense_skill = 2
		planning_skill = 4
		logistics_skill = 4
	}
	create_corps_commander = {
		name = "Władysław Bortnowski"
	    id = 213013
		GFX = GFX_M_Wladyslaw_Bortnowski
		traits = {  media_personality politically_connected organizer }
		skill = 2
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Janusz Gasiorowski"
	    id = 213014
		GFX = GFX_M_Janusz_Gasiorowski
		traits = {  trickster }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Władysław Anders"
	    id = 213015
		GFX = GFX_M_Wladyslaw_Anders
		traits = {  media_personality cavalry_leader war_hero }
		skill = 2
		attack_skill = 4
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Wilhelm Orlik-Rückemann"
	    id = 213016
		GFX = GFX_M_Wilhelm_Orlik-Ruckemann
		traits = {  panzer_leader commando }
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Emil Krukowicz-Przedrzymirski"
	    id = 213017
		GFX = GFX_M_Emil_Krukowicz-Przedrzymirski
		traits = {  organizer inflexible_strategist }
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 2
	}
	create_corps_commander = {
		name = "Mieczysław Boruta-Spiechowicz"
	    id = 213018
		GFX = GFX_M_Mieczysaw_Boruta-Spiechowicz
		traits = {  trait_mountaineer }
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
}
#########################################################################
# Poland - 1936
#########################################################################
1936.1.1 = {
	oob = "POL_1936"
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1935.9.15"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 2
		authoritarian = 70
		democratic = 20
		socialist = 1
		communist = 7
	}
	### Great Depression/Unemployment
	set_variable = { unemployment_value_modifier = 0 }
	set_variable = { current_unemployment = 0.32 }
	clamp_variable = {
		var = current_unemployment
		min = 0
		max = 1
	}
	add_ideas = {
		# Spirit
		# Cabinet
		POL_D_Marian_ZyndramKoscialkowski
		POL_FM_Jozef_Beck
		POL_AM_Eugeniusz_Kwiatkowski
		POL_MoS_Wladyslaw_Raczkiewicz
		# POL_HoI_Janusz_Jagrym_Maleszewski
		industrializing_economy
		limited_civil_liberties
		taxation_level_4
		social_spending_level_3
		research_spending_level_1
		army_spending_level_2
		# Military Staff
		POL_CoStaff_Waclaw_Stachiewicz
		POL_CoArmy_Edward_RydzSmigly
		POL_CoNavy_Jerzy_Swirski
		POL_CoAir_Ludomil_Rayski
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Boleslaw Piasecki"
		desc = ""
		picture = GFX_P_F_Bolesaw_Piasecki
		expire = "1965.1.1"
		ideology = falangism
		traits = { POSITION_President SUBIDEOLOGY_Falangism L_Barking_Buffoon }
	}
	# Authoritarian
	create_country_leader = {
		name = "Ignacy Moscicki"
		desc = ""
		picture = GFX_P_A_Ignacy_Moscicki
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { POSITION_President SUBIDEOLOGY_Authoritarian_Democracy L_Stern_Imperialist }
	}
	#######################
	# Generals
	#######################
	remove_unit_leader = 3150
}
#######################
# Variants
#######################
create_equipment_variant = {
	name = "PZL P.24"
	type = Fighter_equipment_1933
	upgrades = {
		plane_gun_upgrade = 3
		plane_range_upgrade = 0
		plane_engine_upgrade = 1
		plane_reliability_upgrade = 3
	}
}
#########################################################################
# Poland - 1939
#########################################################################
1939.9.1 = {
	oob = "POL_1939"
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1938.9.15"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 2
		authoritarian = 70
		democratic = 20
		socialist = 1
		communist = 7
	}
	add_ideas = {
		# Laws and Policies
		limited_exports
		one_year_service
		partial_economic_mobilisation
		# Cabinet
		POL_D_Felicjan_SlawojSkladkowski
		POL_FM_Jozef_Beck
		POL_AM_Eugeniusz_Kwiatkowski
		POL_MoS_Felicjan_SlawojSkladkowski
		POL_HoI_Kordian_Zamorski
		# Military Staff
		POL_CoStaff_Waclaw_Stachiewicz
		POL_CoArmy_Edward_RydzSmigly
		POL_CoNavy_Jerzy_Swirski
		POL_CoAir_Ludomil_Rayski
	}
	#######################
	# Leaders
	#######################
	# Democracy
	create_country_leader = {
		name = "Tadeusz Bielecki"
		desc = ""
		picture = GFX_P_D_Tadeusz_Bielecki
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Social_Conservatism L_Weary_Stiff_Neck }
	}
	# Socialism
	create_country_leader = {
		name = "Kazimierz Puzak"
		desc = ""
		picture = GFX_P_S_Kazimierz_Puzak
		expire = "1965.1.1"
		ideology = socialism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Socialism L_Popular_Figurehead }
	}
	#######################
	# Generals
	#######################
	create_corps_commander = {
		name = "Roman Abraham"
	    id = 213019
		GFX = GFX_M_Roman_Abraham
		traits = { brilliant_strategist cavalry_officer cavalry_leader trickster }
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 4
	}
	create_corps_commander = {
		name = "Mikołaj Bołtuć"
	    id = 213020
		GFX = GFX_M_Mikolaj_Boltuc
		traits = {  trait_reckless ranger }
		skill = 2
		attack_skill = 3
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 1
	}
	create_corps_commander = {
		name = "Zygmunt Podhorski"
	    id = 213021
		GFX = GFX_M_Zygmunt_Podhorski
		traits = {  cavalry_officer organizer swamp_fox }
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 3
	}
	create_corps_commander = {
		name = "Wincenty Kowalski"
	    id = 213022
		GFX = GFX_M_Wincenty_Kowalski
		traits = {  infantry_leader trickster }
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
}
######################################################################
# Poland - 1940
######################################################################
1940.5.10 = {
	oob = "POL_1940"
}
######################################################################
# Poland - 1944
######################################################################
1944.6.20 = {
	oob = "POL_1944"
}
######################################################################
# Poland - 1946
######################################################################
1946.1.1 = {
	oob = "POL_1946"
	#######################
	# Politics
	#######################
	set_cosmetic_tag = POL_SOV_1946
	set_politics = {
		ruling_party = communist
		last_election = "1935.9.15"
		election_frequency = 36
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 0
		democratic = 0
		socialist = 0
		communist = 100
	}
}
######################################################################
# Poland - 1950
######################################################################
1950.1.1 = {
	#######################
	# Politics
	#######################
	set_cosmetic_tag = POL_SOV
}
