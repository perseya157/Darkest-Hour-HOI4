﻿division_template = {
	name = "Tmíma pezikoú" 				# Infantry Division
	division_names_group = GRE_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Tmíma pezikoú me pyrovolikó" 				# Infantry Division with Artillery
	division_names_group = GRE_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		artillery_battalion = { x = 2 y = 0 }
		artillery_battalion = { x = 2 y = 1 }
		artillery_battalion = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Tmíma pezikoú me antipyravliká" 				# Infantry Division with Anti Air
	division_names_group = GRE_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		anti_air_brigade = { x = 2 y = 0 }
		anti_air_brigade = { x = 2 y = 1 }
		anti_air_brigade = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Merarchía Ippikoú me Pyrovolikoú" 			# Cavalry Division with Artillery
	division_names_group = GRE_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		artillery_battalion = { x = 2 y = 0 }
		artillery_battalion = { x = 2 y = 1 }
	}
}

units = {
	##### I. Soma Stratou #####
	division = { # 2. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 4109 # Athens
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 3. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 4109 # Athens
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 4. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 4109 # Athens
        division_template = "Tmíma pezikoú me pyrovolikó"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}
	##### II. Soma Stratou #####
	division = { # 1. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 1109 # Levadhia
		division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 9. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 1109 # Levadhia
		division_template = "Tmíma pezikoú me antipyravliká"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}
	##### III. Soma Stratou #####
	division = { # 6. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 11818 # Thessaloniki
        division_template = "Tmíma pezikoú me pyrovolikó"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 10. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 11818 # Thessaloniki
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 11. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 11818 # Thessaloniki
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 17. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}
		location = 11818 # Thessaloniki
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}
	##### IV. Soma Stratou #####
	division = { # 7. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 1109 # Levadhia
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 14. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}
		location = 1109 # Levadhia
        division_template = "Tmíma pezikoú me pyrovolikó"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}
	##### V. Soma Stratou #####
	division = { # 12. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
		location = 3914 # Ioannina
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 13. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 3914 # Ioannina
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}
	##### VI. Soma Stratou #####
	division = { # 5. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 9940 # Irakleio
        division_template = "Tmíma pezikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 8. Merarchia Pezikou
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 9940 # Irakleio
        division_template = "Tmíma pezikoú me pyrovolikó"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}

	division = { # 1. Merarchia Ippikou
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 9940 # Irakleio
        division_template = "Merarchía Ippikoú me Pyrovolikoú"
        start_experience_factor = 0.2
        start_equipment_factor = 0.4
	}


	##### NAVAL UNITS #####
	fleet = {
		name = "Ellinikos Vasilikos Stolos"
		naval_base = 4109 # Athens
		task_force = {
			name = "Ellinikos Vasilikos Stolos"
			location = 4109 # Athens
			ship = { name = "3. Stoliskos Antitorpillikon" definition = destroyer equipment = { DD_equipment_1900 = { amount = 1 owner = GRE } } }		
			ship = { name = "4. Stoliskos Antitorpillikon" definition = destroyer equipment = { DD_equipment_1885 = { amount = 1 owner = GRE } } }	
		}
		task_force = {
			name = "Polemikos Stolos"
			location =  4109 # Athens
			ship = { name = "BEN Georgios Averoff" definition = heavy_cruiser equipment = { CA_equipment_1895 = { amount = 1 owner = GRE } } }				
			ship = { name = "BEN Helle" definition = light_cruiser equipment = { CL_equipment_1885 = { amount = 1 owner = GRE } } }				
			ship = { name = "1. Stoliskos Antitorpillikon" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = GRE } } }		
			ship = { name = "2. Stoliskos Antitorpillikon" definition = destroyer equipment = { DD_equipment_1912 = { amount = 1 owner = GRE } } }
		}
		task_force = {
			name = "Stolískou Ypovrychíon"
			location = 4109 # Athens
			ship = { name = "1. Ypobrychio" definition = submarine equipment = { SS_equipment_1922 = { amount = 1 owner = GRE } } }
		}
	}	
}

air_wings = {
	# Polemiki Aeroporia
	47 = {
		Fighter_equipment_1933 = { # Bristol Scout A
			owner = "GRE"
			amount = 75
		}
	}
}


### Starting Production ###
instant_effect = {
    add_equipment_production = {
        equipment = {
            type = Small_Arms_equipment_1936
            creator = "GRE"
        }
        requested_factories = 2
        progress = 0.3
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = support_equipment_1
            creator = "GRE"
        }
        requested_factories = 1
        progress = 0.72
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = artillery_equipment_1
            creator = "GRE"
        }
        requested_factories = 1
        progress = 0.24
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = DD_equipment_1933
            creator = "GRE"
        }
        requested_factories = 1
        progress = 0.18
        amount = 2
    }
}