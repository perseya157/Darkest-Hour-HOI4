﻿division_template = {
	name = "Pěší Divise"					# Infantry Division
	division_names_group = CZE_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		artillery_battalion = { x = 4 y = 0 }
		artillery_battalion = { x = 4 y = 1 }
		artillery_battalion = { x = 4 y = 2 }
	}
	support = {
		field_hospital = { x = 0 y = 0 }
		logistics_company = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Pěší Divise (Hraničářské Prapory)"					# Infantry Division with border battalions attached
	division_names_group = CZE_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		light_infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		light_infantry = { x = 2 y = 3 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		artillery_battalion = { x = 4 y = 0 }
		artillery_battalion = { x = 4 y = 1 }
		artillery_battalion = { x = 4 y = 2 }
	}
	support = {
		field_hospital = { x = 0 y = 0 }
		logistics_company = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Pěší Divise (Hraničářský Prapor)"					# Infantry Division with border battalion attached
	division_names_group = CZE_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		light_infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 3 y = 0 }
		infantry = { x = 3 y = 1 }
		infantry = { x = 3 y = 2 }
		artillery_battalion = { x = 4 y = 0 }
		artillery_battalion = { x = 4 y = 1 }
		artillery_battalion = { x = 4 y = 2 }
	}
	support = {
		field_hospital = { x = 0 y = 0 }
		logistics_company = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Horská Brigáda"					# Mountain Brigade
	division_names_group = CZE_MTNB_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		artillery_battalion_light = { x = 2 y = 0 }
		artillery_battalion_light = { x = 2 y = 1 }
	}
}
division_template = {
	name = "Jezdecká Brigáda"	#Cavalry Brigade
	division_names_group = CZE_CAV_01
    
    regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 0 y = 3 }
		artillery_battalion_light = { x = 1 y = 0 }
		bicycle_battalion = { x = 2 y = 0 } # Added in September 1933
	}
	support = {
		recon = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brigáda Útočné Vozby"	#Armored Brigade
	division_names_group = CZE_QUI_01	
	regiments = {
		medium_armor = { x = 0 y = 0 } #Light Tanks
		medium_armor = { x = 0 y = 1 } #Mixed Battalion Tanks
		medium_armor = { x = 0 y = 2 } #Light Infantry Tanks
		medium_armor = { x = 0 y = 3 } #Medium Tanks
		medium_armor = { x = 1 y = 0 } #Light Cavalry Tanks
		medium_armor = { x = 1 y = 1 } #Light Infantry Tanks
		medium_armor = { x = 1 y = 2 } #Medium Tanks
	}
	support = {
		armored_car_recon = { x = 0 y = 0 }
	}
}

units = {
	##Infantry
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
        location = 11542 # Prague
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
        location = 6418 # Plzeň
        division_template = "Pěší Divise (Hraničářské Prapory)"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
        location = 14680 # Litoměřice
        division_template = "Pěší Divise (Hraničářský Prapor)"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
        location = 583 # Hradec Králové
        division_template = "Pěší Divise (Hraničářský Prapor)"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
        location = 13657 # České Budějovice
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
        location = 3569 # Brno
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
        location = 3553 # Olomouc
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
        location = 15104 # Hranice
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
        location = 9692 # Bratislava
        division_template = "Pěší Divise (Hraničářský Prapor)"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
        location = 3537 # Banská Bystrica
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
        location = 6573 # Košice
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
        location = 13634 # Užhorod
        division_template = "Pěší Divise"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
	##Mountain
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
        location = 11554 # Ružomberok
        division_template = "Horská Brigáda"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
        location = 3550 # Spišská Nová Ves
        division_template = "Horská Brigáda"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
	##Cavalry
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
        location = 11542 # Prague
        division_template = "Jezdecká Brigáda"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
        location = 3569 # Brno
        division_template = "Jezdecká Brigáda"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
        location = 9692 # Bratislava
        division_template = "Jezdecká Brigáda"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
    division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
        location = 6573 # Košice
        division_template = "Jezdecká Brigáda"
        start_experience_factor = 0.22 # Reforms begun in 1934 meant qualitative improvement of the Czech military
        start_equipment_factor = 0.8 # No major equipment shortages noted in my research 
    }
	##Armor
	division= {
		name = "Brigáda útočné vozby"
		location = 9429 # Milovice
		division_template = "Brigáda Útočné Vozby"
        start_experience_factor = 0.10 # A new formation
        start_equipment_factor = 0.5 # At this date they were still using a lot of WWI-era training tanks while production of new models was underway.
	}
}

### Air Wings
air_wings = {
	# Letectvo I.Armády -- Prague
	# Letectvo Hlavniho Velitelstivi -- Prague
	9 = { 
		Fighter_equipment_1933 = {		# Avia B.534
			owner = "CZE" 
			amount = 36
		}
		Tactical_Bomber_equipment_1933 =  {	# Bloch MB.200
			owner = "CZE" 
			amount = 72
		}
		Attacker_equipment_1 =  {		# Aero A-101
			owner = "CZE" 
			amount = 24
		}
	}

	# Letectvo III.Armády -- Bratislava
	70 = {
		Fighter_equipment_1933 = {		# Avia B.534
			owner = "CZE" 
			amount = 36
		}
	}

	# Letectvo IV.Armády -- Olomouc
	75 = {
		Fighter_equipment_1933 = {		# Avia B.534
			owner = "CZE" 
			amount = 72
		}
		Attacker_equipment_1 =  {		# Aero A-101
			owner = "CZE" 
			amount = 24
		}
	}
}

### Starting Production ###
instant_effect = {
    add_equipment_production = {
        equipment = {
            type = Small_Arms_equipment_1936
            creator = "CZE"
        }
        requested_factories = 3
        progress = 0.38
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = support_equipment_1
            creator = "CZE"
        }
        requested_factories = 1
        progress = 0.7
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = artillery_equipment_1
            creator = "CZE"
        }
        requested_factories = 2
        progress = 0.3
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = Anti_Aircraft_Gun_equipment_1935
            creator = "CZE"
        }
        requested_factories = 1
        progress = 0.29
        efficiency = 100
    }

    add_equipment_production = {
        equipment = {
            type = Light_Tank_equipment_1936
            creator = "CZE"
        }
        requested_factories = 2
        progress = 0.18
        efficiency = 100
    }
}